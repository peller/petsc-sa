
/*
    This file implements the conjugate gradient method in PETSc as part of
    KSP. You can use this as a starting point for implementing your own
    Krylov method that is not provided with PETSc.

    This implementation uses optimized vector operations and reductions for
    improved performance. Contains most of the functionality from standard
    PETSc CG implementation, but may not have some features.
    Reference: Eller and Gropp, 2016.

    The following basic routines are required for each Krylov method.
        KSPCreate_XXX()          - Creates the Krylov context
        KSPSetFromOptions_XXX()  - Sets runtime options
        KSPSolve_XXX()           - Runs the Krylov method
        KSPDestroy_XXX()         - Destroys the Krylov context, freeing all
                                   memory it needed
    Here the "_XXX" denotes a particular implementation, in this case
    we use _CG (e.g. KSPCreate_SACG, KSPDestroy_SACG). These routines are
    are actually called via the common user interface routines
    KSPSetType(), KSPSetFromOptions(), KSPSolve(), and KSPDestroy() so the
    application code interface remains identical for all preconditioners.

    Other basic routines for the KSP objects include
        KSPSetUp_XXX()
        KSPView_XXX()            - Prints details of solver being used.

    Detailed notes:
    By default, this code implements the CG (Conjugate Gradient) method,
    which is valid for real symmetric (and complex Hermitian) positive
    definite matrices. Note that for the complex Hermitian case, the
    VecDot() arguments within the code MUST remain in the order given
    for correct computation of inner products.

    Reference: Hestenes and Steifel, 1952.

    By switching to the indefinite vector inner product, VecTDot(), the
    same code is used for the complex symmetric case as well.  The user
    must call KSPCGSetType(ksp,KSP_CG_SYMMETRIC) or use the option
    -ksp_cg_type symmetric to invoke this variant for the complex case.
    Note, however, that the complex symmetric code is NOT valid for
    all such matrices ... and thus we don't recommend using this method.
*/
/*
    cgimpl.h defines the simple data structured used to store information
    related to the type of matrix (e.g. complex symmetric) being solved and
    data used during the optional Lanczo process used to compute eigenvalues
*/
#include <../src/ksp/ksp/impls/cg/cgimpl.h>       /*I "petscksp.h" I*/
#include <sa_app.h>

/*
     KSPSetUp_SACG - Sets up the workspace needed by the CG method.

      This is called once, usually automatically by KSPSolve() or KSPSetUp()
     but can be called directly by KSPSetUp()
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSetUp_SACG"
static PetscErrorCode KSPSetUp_SACG(KSP ksp)
{
  KSP_CG         *cgP = (KSP_CG*)ksp->data;
  PetscErrorCode ierr;
  PetscInt       nwork = 3;

  PetscFunctionBegin;
  /* get work vectors needed by CG */
  if (cgP->singlereduction) nwork += 2;
  ierr = KSPSetWorkVecs(ksp,nwork);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
     KSPSolve_SACG - This routine actually applies the conjugate gradient method

     Note : this routine can be replaced with another one (see below) which implements
            another variant of CG.

   Input Parameter:
.     ksp - the Krylov space object that was set to use conjugate gradient, by, for
            example, KSPCreate(MPI_Comm,KSP *ksp); KSPSetType(ksp,KSPCG);
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSolve_SACG"
static PetscErrorCode KSPSolve_SACG(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i,stored_max_it,eigs,allr_type=0;
  PetscScalar    dpi=0.0,a=1.0,beta,betaold=1.0,b=0,*e=0,*d=0,dpiold,mpivals[2];
  PetscReal      dp=0.0;
  Vec            X,B,Z,R,P,W;
  KSP_CG         *cg;
  Mat            Amat,Pmat;
  PetscBool      diagonalscale;
  MPI_Comm       pcomm;
  MPI_Status     stat;
  MPI_Request    req;

  PetscFunctionBegin;
  // Get allr_type option
  ierr = PetscOptionsGetInt(NULL,NULL,"-allr_type",&allr_type,NULL);CHKERRQ(ierr);

  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);
  pcomm = PetscObjectComm((PetscObject)ksp);
  ierr  = PCGetDiagonalScale(ksp->pc,&diagonalscale);CHKERRQ(ierr);
  if (diagonalscale) SETERRQ1(pcomm,PETSC_ERR_SUP,"Krylov method %s does not support diagonal scaling",
                              ((PetscObject)ksp)->type_name);

  // Check if normtype is supported
  if(ksp->normtype!=KSP_NORM_PRECONDITIONED && ksp->normtype!=KSP_NORM_UNPRECONDITIONED &&
     ksp->normtype!=KSP_NORM_NATURAL && ksp->normtype!=KSP_NORM_NONE) {
    SETERRQ1(pcomm,PETSC_ERR_SUP,"%s",KSPNormTypes[ksp->normtype]);
  }

  cg            = (KSP_CG*)ksp->data;
  eigs          = ksp->calc_sings;
  stored_max_it = ksp->max_it;

  X = ksp->vec_sol; B = ksp->vec_rhs;
  R = ksp->work[0]; Z = ksp->work[1];
  P = ksp->work[2]; W = Z;

  if (eigs) {e = cg->e; d = cg->d; e[0] = 0.0; }
  ierr = PCGetOperators(ksp->pc,&Amat,&Pmat);CHKERRQ(ierr);

  ksp->its = 0;
  if (!ksp->guess_zero) {
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,X,R),NTD_MV);CHKERRQ(ierr); // r <- b - Ax
    NT_ITER_FUNC(ierr = VecAYPX(R,-1.0,B),NTD_VECOPSPC);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = VecCopy(B,R),NTD_VECOPSPC);CHKERRQ(ierr);          // r <- b (x is 0)
  }

  // Compute preconditioner
  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R,Z),NTD_PC);CHKERRQ(ierr);        // z <- Br

  /***********************************************************
   * Compute merged vector operations
   * beta = z'*r
   * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
   *      or beta (Natural)
   **********************************************************/
  NT_ITER_FUNC(ierr = VecMergedDot_CG(Z,R,ksp->normtype,&beta,&dp),NTD_VECOPSPC);CHKERRQ(ierr);

  // Compute allreduce
  mpivals[0] = beta;
  mpivals[1] = dp;
  if(allr_type==1) {
    NT_ITER_START(NTD_PCALLR);
    ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,
                          MPI_SUM,pcomm,&req);CHKERRQ(ierr);
    ierr = MPI_Wait(&req,&stat);CHKERRQ(ierr);
    NT_ITER_END(NTD_PCALLR);
  } else {
    NT_ITER_FUNC(ierr = MPI_Allreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,
                                      MPI_SUM,pcomm),NTD_PCALLR);CHKERRQ(ierr);
  }
  beta     = mpivals[0];
  dp       = PetscSqrtReal(PetscAbsScalar(mpivals[1]));

  // Set convergence information
  ierr       = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr       = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ksp->rnorm = dp;
  ierr = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

  // Set comm timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
              NT_ITER_TIME(NTD_MVALLR)+NT_ITER_TIME(NTD_PCALLR),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_PCALLR),NTD_ALLR);

  // Set comp timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
              NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPSPC),NTD_COMP);
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSPC),NTD_VECOPS);

  // Set allreduce comp timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSPC)+NT_ITER_TIME(NTD_PC),NTD_PCACOMP);

  // Set total timer
  NT_ITER_END(NTD_TOTAL);
  NT_ITER_FINISH();

  if (ksp->reason) PetscFunctionReturn(0);

  // Main iteration loop
  i = 0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);
    if (beta == 0.0) {
      ksp->reason = KSP_CONVERGED_ATOL;
      ierr        = PetscInfo(ksp,"converged due to beta = 0\n");CHKERRQ(ierr);
      break;
#if !defined(PETSC_USE_COMPLEX)
    } else if ((i > 0) && (beta*betaold < 0.0)) {
      ksp->reason = KSP_DIVERGED_INDEFINITE_PC;
      ierr        = PetscInfo(ksp,"diverging due to indefinite preconditioner\n");CHKERRQ(ierr);
      break;
#endif
    }

    if (!i) {
      NT_ITER_FUNC(ierr = VecCopy(Z,P),NTD_VECOPSMV);CHKERRQ(ierr); // p <- z
      b = 0.0;
    } else {
      b = beta/betaold;
      if (eigs) {
        if (ksp->max_it != stored_max_it)
          SETERRQ(pcomm,PETSC_ERR_SUP,"Can not change maxit AND calculate eigenvalues");
        e[i] = PetscSqrtReal(PetscAbsScalar(b))/a;
      }
      /************************************
       * Compute merged vector operations:
       * x <- x + ap
       * p <- z + b* p
       ***********************************/
      NT_ITER_FUNC(ierr = VecMergedOps_CG(X,P,Z,a,b),NTD_VECOPSMV);CHKERRQ(ierr);
    }
    dpiold  = dpi;
    betaold = beta;

    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,P,W),NTD_MV);CHKERRQ(ierr);  // w <- Ap
    NT_ITER_FUNC(ierr = VecLocalDot(P,W,&dpi),NTD_VECOPSMV);CHKERRQ(ierr); // dpi <- p'w

    mpivals[0] = dpi;
    if(allr_type==1) {
      NT_ITER_START(NTD_MVALLR);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,1,MPI_DOUBLE,
                            MPI_SUM,pcomm,&req);CHKERRQ(ierr);
      ierr = MPI_Wait(&req,&stat);CHKERRQ(ierr);
      NT_ITER_END(NTD_MVALLR);
    } else {
      NT_ITER_FUNC(ierr = MPI_Allreduce(MPI_IN_PLACE,mpivals,1,MPI_DOUBLE,
                                        MPI_SUM,pcomm),NTD_MVALLR);CHKERRQ(ierr);
    }
    dpi = mpivals[0];

    if ((dpi == 0.0) || ((i > 0) && (PetscRealPart(dpi*dpiold) <= 0.0))) {
      ksp->reason = KSP_DIVERGED_INDEFINITE_MAT;
      ierr        = PetscInfo(ksp,"diverging due to indefinite or negative definite matrix\n");CHKERRQ(ierr);
      break;
    }

    a = beta/dpi;                                // a = beta/p'w
    if (eigs) d[i] = PetscSqrtReal(PetscAbsScalar(b))*e[i] + 1.0/a;

    NT_ITER_FUNC(ierr = VecAXPY(R,-a,W),NTD_VECOPSPC);CHKERRQ(ierr);  // r <- r - aw
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R,Z),NTD_PC);CHKERRQ(ierr); // z <- Br

    // Compute merged vecops
    NT_ITER_FUNC(ierr = VecMergedDot_CG(Z,R,ksp->normtype,&beta,&dp),NTD_VECOPSPC);CHKERRQ(ierr);

    // Compute allreduce
    mpivals[0] = beta; mpivals[1] = dp;
    if(allr_type==1) {
      NT_ITER_START(NTD_PCALLR);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,
                            MPI_SUM,pcomm,&req);CHKERRQ(ierr);
      ierr = MPI_Wait(&req,&stat);CHKERRQ(ierr);
      NT_ITER_END(NTD_PCALLR);
    } else {
      NT_ITER_FUNC(ierr = MPI_Allreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,
                                        MPI_SUM,pcomm),NTD_PCALLR);CHKERRQ(ierr);
    }
    beta     = mpivals[0];
    dp       = PetscSqrtReal(PetscAbsScalar(mpivals[1]));

    // Set convergence information
    i++;
    ksp->its = i;
    ksp->rnorm = dp;
    ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    if (eigs) cg->ned = ksp->its;
    ierr = KSPMonitor(ksp,i,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

    // Compute final x vector
    if(ksp->reason || i>=ksp->max_it) {
      ierr = VecAXPY(X,a,P);CHKERRQ(ierr); // x <- x + ap
    }

    // Set comm timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVALLR)+NT_ITER_TIME(NTD_PCALLR),NTD_ALLR);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
                NT_ITER_TIME(NTD_MVALLR)+NT_ITER_TIME(NTD_PCALLR),NTD_COMM);

    // Set comp timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC)+
                NT_ITER_TIME(NTD_VECOPSMV)+NT_ITER_TIME(NTD_VECOPSPC),NTD_COMP);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSMV)+NT_ITER_TIME(NTD_VECOPSPC),NTD_VECOPS);

    // Set allreduce comp timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSMV)+NT_ITER_TIME(NTD_MV_DIAG)+
                NT_ITER_TIME(NTD_MV_OFFD),NTD_MVACOMP);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSPC)+NT_ITER_TIME(NTD_PC),NTD_PCACOMP);

    // Set total timer
    NT_ITER_END(NTD_TOTAL);
    NT_ITER_FINISH();

    if (ksp->reason) break;

  } while (i<ksp->max_it);
  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;
  PetscFunctionReturn(0);
}

/*
       KSPSolve_SACG_SingleReduction

       This variant of CG is identical in exact arithmetic to the standard algorithm,
       but is rearranged to use only a single reduction stage per iteration, using additional
       intermediate vectors.

       See KSPCGUseSingleReduction_CG()

       This implementation uses optimized vector operations and reductions for
       improved performance. Contains most of the functionality from standard
       PETSc CG implementation, but may not have some features.
       Reference: Eller and Gropp, 2016.

*/
#undef __FUNCT__
#define __FUNCT__ "KSPSolve_SACG_SingleReduction"
static PetscErrorCode KSPSolve_SACG_SingleReduction(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i,stored_max_it,eigs;
  PetscScalar    dpi = 0.0,a = 1.0,beta,betaold = 1.0,b = 0,*e = 0,*d = 0;
  PetscScalar    delta,dpiold,mpivals[3];
  PetscReal      dp  = 0.0;
  Vec            X,B,Z,R,P,S,W;
  KSP_CG         *cg;
  Mat            Amat,Pmat;
  PetscBool      diagonalscale;
  MPI_Comm       pcomm;

  PetscFunctionBegin;
  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);
  pcomm = PetscObjectComm((PetscObject)ksp);
  ierr  = PCGetDiagonalScale(ksp->pc,&diagonalscale);CHKERRQ(ierr);
  if (diagonalscale) SETERRQ1(pcomm,PETSC_ERR_SUP,"Krylov method %s does not support diagonal scaling",
                              ((PetscObject)ksp)->type_name);

  // Check if normtype is supported
  if(ksp->normtype!=KSP_NORM_PRECONDITIONED && ksp->normtype!=KSP_NORM_UNPRECONDITIONED &&
     ksp->normtype!=KSP_NORM_NATURAL && ksp->normtype!=KSP_NORM_NONE) {
    SETERRQ1(pcomm,PETSC_ERR_SUP,"%s",KSPNormTypes[ksp->normtype]);
  }

  cg            = (KSP_CG*)ksp->data;
  eigs          = ksp->calc_sings;
  stored_max_it = ksp->max_it;

  X = ksp->vec_sol; B = ksp->vec_rhs;
  R = ksp->work[0]; Z = ksp->work[1];
  P = ksp->work[2]; S = ksp->work[3];
  W = ksp->work[4];

  if (eigs) {e = cg->e; d = cg->d; e[0] = 0.0; }
  ierr = PCGetOperators(ksp->pc,&Amat,&Pmat);CHKERRQ(ierr);

  ksp->its = 0;
  if (!ksp->guess_zero) {
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,X,R),NTD_MV);CHKERRQ(ierr); // r <- b - Ax
    NT_ITER_FUNC(ierr = VecAYPX(R,-1.0,B),NTD_VECOPS);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = VecCopy(B,R),NTD_VECOPS);CHKERRQ(ierr);          // r <- b (x is 0)
  }

  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R,Z),NTD_PC);CHKERRQ(ierr);      // z <- Mr
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,Z,S),NTD_MV);CHKERRQ(ierr); // s <- Az

  /**********************************************************
   * Compute:
   * delta = z'*s (z'*A*z = r'*B*A*B*r)
   * beta  = z'*r
   * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
   *      or sqrt(beta) (Natural)
   *********************************************************/
  NT_ITER_FUNC(ierr = VecMergedDot_SAPCG(Z,S,R,ksp->normtype,&delta,&beta,&dp),NTD_VECOPS);CHKERRQ(ierr);

  mpivals[0] = delta; mpivals[1] = beta; mpivals[2] = dp;
  NT_ITER_FUNC(ierr = MPI_Allreduce(MPI_IN_PLACE,mpivals,3,MPI_DOUBLE,MPI_SUM,pcomm),NTD_ALLR);CHKERRQ(ierr);
  delta    = mpivals[0]; beta = mpivals[1];
  dp       = PetscSqrtReal(PetscAbsScalar(mpivals[2]));

  // Set convergence information
  ierr       = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr       = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ksp->rnorm = dp;
  ierr       = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

  // Set comm and total timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
              NT_ITER_TIME(NTD_ALLR),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
              NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPS),NTD_COMP);

  NT_ITER_END(NTD_TOTAL);
  NT_ITER_FINISH();

  if (ksp->reason) PetscFunctionReturn(0);

  i = 0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);
    if (beta == 0.0) {
      ksp->reason = KSP_CONVERGED_ATOL;
      ierr        = PetscInfo(ksp,"converged due to beta = 0\n");CHKERRQ(ierr);
      break;
#if !defined(PETSC_USE_COMPLEX)
    } else if ((i > 0) && (beta*betaold < 0.0)) {
      ksp->reason = KSP_DIVERGED_INDEFINITE_PC;
      ierr        = PetscInfo(ksp,"diverging due to indefinite preconditioner\n");CHKERRQ(ierr);
      break;
#endif
    }

    if (!i) b = 0.0;
    else {
      b = beta/betaold;
      if (eigs) {
        if (ksp->max_it != stored_max_it)
          SETERRQ(pcomm,PETSC_ERR_SUP,"Can not change maxit AND calculate eigenvalues");
        e[i] = PetscSqrtReal(PetscAbsScalar(b))/a;
      }
    }

    dpiold = dpi;
    dpi  = delta - beta*beta*dpiold/(betaold*betaold); // dpi <- p'w
    if ((dpi == 0.0) || ((i > 0) && (PetscRealPart(dpi*dpiold) <= 0.0))) {
      ksp->reason = KSP_DIVERGED_INDEFINITE_MAT;
      ierr        = PetscInfo(ksp,"diverging due to indefinite or negative definite matrix\n");CHKERRQ(ierr);
      break;
    }
    a = beta/dpi;                                      // a = beta/p'w
    if (eigs) d[i] = PetscSqrtReal(PetscAbsScalar(b))*e[i] + 1.0/a;

    /************************************************
     * Compute merged vector operations for SAPCG:
     * p <- z + b*p
     * w <- s + b*w
     * x <- x + a*p
     * r <- r - a*w
     ***********************************************/
    NT_ITER_FUNC(ierr = VecMergedOps_SAPCG(P,Z,W,S,X,R,b,a),NTD_VECOPS);CHKERRQ(ierr);

    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R,Z),NTD_PC);CHKERRQ(ierr);      // z <- Mr
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,Z,S),NTD_MV);CHKERRQ(ierr); // s <- Az

    /***********************************************************
     * Compute:
     * delta = z'*s (z'*A*z = r'*B*A*B*r)
     * beta  = z'*r
     * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
     *      or sqrt(beta) (Natural)
     **********************************************************/
    betaold = beta;
    NT_ITER_FUNC(ierr = VecMergedDot_SAPCG(Z,S,R,ksp->normtype,&delta,&beta,&dp),NTD_VECOPS);CHKERRQ(ierr);

    // Compute allreduce
    mpivals[0] = delta; mpivals[1] = beta; mpivals[2] = dp;
    NT_ITER_FUNC(ierr = MPI_Allreduce(MPI_IN_PLACE,mpivals,3,MPI_DOUBLE,MPI_SUM,pcomm),NTD_ALLR);CHKERRQ(ierr);
    delta    = mpivals[0]; beta = mpivals[1];
    dp       = PetscSqrtReal(PetscAbsScalar(mpivals[2]));

    // Set convergence information
    i++;
    ksp->its = i;
    ksp->rnorm = dp;
    CHKERRQ(ierr);KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    if (eigs) cg->ned = ksp->its;
    ierr = KSPMonitor(ksp,i,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

    // Set comm and total timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
                NT_ITER_TIME(NTD_ALLR),NTD_COMM);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
                NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPS),NTD_COMP);
    NT_ITER_END(NTD_TOTAL);
    NT_ITER_FINISH();

    if (ksp->reason) break;
  } while (i<ksp->max_it);
  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;
  PetscFunctionReturn(0);
}

/*
     KSPDestroy_SACG - Frees resources allocated in KSPSetup_SACG and clears function
                       compositions from KSPCreate_SACG. If adding your own KSP implementation,
                       you must be sure to free all allocated resources here to prevent
                       leaks.
*/
#undef __FUNCT__
#define __FUNCT__ "KSPDestroy_SACG"
PetscErrorCode KSPDestroy_SACG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPDestroyDefault(ksp);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)ksp,"KSPCGSetType_C",NULL);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)ksp,"KSPCGUseSingleReduction_C",NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
     KSPView_SACG - Prints information about the current Krylov method being used.
                    If your Krylov method has special options or flags that information
                    should be printed here.
*/
#undef __FUNCT__
#define __FUNCT__ "KSPView_SACG"
PetscErrorCode KSPView_SACG(KSP ksp,PetscViewer viewer)
{
  KSP_CG         *cg = (KSP_CG*)ksp->data;
  PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
#if defined(PETSC_USE_COMPLEX)
    ierr = PetscViewerASCIIPrintf(viewer,"  CG or CGNE: variant %s\n",KSPCGTypes[cg->type]);CHKERRQ(ierr);
#endif
    if (cg->singlereduction) {
      ierr = PetscViewerASCIIPrintf(viewer,"  CG: using single-reduction variant\n");CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

/*
    KSPCGUseSingleReduction_SACG

    This routine sets a flag to use a variant of SACG. Note that (in somewhat
    atypical fashion) it also swaps out the routine called when KSPSolve()
    is invoked.
*/
#undef __FUNCT__
#define __FUNCT__ "KSPCGUseSingleReduction_SACG"
static PetscErrorCode  KSPCGUseSingleReduction_SACG(KSP ksp,PetscBool flg)
{
  KSP_CG *cg = (KSP_CG*)ksp->data;

  PetscFunctionBegin;
  cg->singlereduction = flg;
  if (cg->singlereduction) {
    ksp->ops->solve = KSPSolve_SACG_SingleReduction;
  } else {
    ksp->ops->solve = KSPSolve_SACG;
  }
  PetscFunctionReturn(0);
}

/*
    KSPSetFromOptions_SACG - Checks the options database for options related to the
                             conjugate gradient method.
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSetFromOptions_SACG"
PetscErrorCode KSPSetFromOptions_SACG(PetscOptionItems *PetscOptionsObject,KSP ksp)
{
  PetscErrorCode ierr;
  KSP_CG         *cg = (KSP_CG*)ksp->data;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"KSP CG and CGNE options");CHKERRQ(ierr);
#if defined(PETSC_USE_COMPLEX)
  ierr = PetscOptionsEnum("-ksp_cg_type","Matrix is Hermitian or complex symmetric","KSPCGSetType",KSPCGTypes,(PetscEnum)cg->type,
                          (PetscEnum*)&cg->type,NULL);CHKERRQ(ierr);
#endif
  ierr = PetscOptionsBool("-ksp_cg_single_reduction","Merge inner products into single MPIU_Allreduce()","KSPCGUseSingleReduction",cg->singlereduction,&cg->singlereduction,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsTail();CHKERRQ(ierr);

  ierr = KSPCGUseSingleReduction_SACG(ksp,cg->singlereduction);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
    KSPCGSetType_SACG - This is an option that is SPECIFIC to this particular Krylov method.
                        This routine is registered below in KSPCreate_SACG() and called from the
                        routine KSPCGSetType() (see the file cgtype.c).
*/
#undef __FUNCT__
#define __FUNCT__ "KSPCGSetType_SACG"
PetscErrorCode  KSPCGSetType_SACG(KSP ksp,KSPCGType type)
{
  KSP_CG *cg = (KSP_CG*)ksp->data;

  PetscFunctionBegin;
  cg->type = type;
  PetscFunctionReturn(0);
}

/*
    KSPCreate_SACG - Creates the data structure for the Krylov method CG and sets the
       function pointers for all the routines it needs to call (KSPSolve_CG() etc)

    It must be labeled as PETSC_EXTERN to be dynamically linkable in C++
*/
/*MC
     KSPSACG - The preconditioned conjugate gradient (PCG) iterative method

   Options Database Keys:
+   -ksp_cg_type Hermitian - (for complex matrices only) indicates the matrix is Hermitian, see KSPCGSetType()
.   -ksp_cg_type symmetric - (for complex matrices only) indicates the matrix is symmetric
-   -ksp_cg_single_reduction - performs both inner products needed in the algorithm with a single MPIU_Allreduce() call, see KSPCGUseSingleReduction()

   Level: beginner

   Notes: The PCG method requires both the matrix and preconditioner to be symmetric positive (or negative) (semi) definite
          Only left preconditioning is supported.

   For complex numbers there are two different CG methods. One for Hermitian symmetric matrices and one for non-Hermitian symmetric matrices. Use
   KSPCGSetType() to indicate which type you are using.

   Developer Notes: KSPSolve_SACG() should actually query the matrix to determine if it is Hermitian symmetric or not and NOT require the user to
   indicate it to the KSP object.

   References:
.   1. - Magnus R. Hestenes and Eduard Stiefel, Methods of Conjugate Gradients for Solving Linear Systems,
   Journal of Research of the National Bureau of Standards Vol. 49, No. 6, December 1952 Research Paper 2379

.seealso:  KSPCreate(), KSPSetType(), KSPType (for list of available types), KSP,
           KSPCGSetType(), KSPCGUseSingleReduction(), KSPPIPECG, KSPGROPPCG

M*/
#undef __FUNCT__
#define __FUNCT__ "KSPCreate_SACG"
PETSC_EXTERN PetscErrorCode KSPCreate_SACG(KSP ksp)
{
  PetscErrorCode ierr;
  KSP_CG         *cg;

  PetscFunctionBegin;
  ierr = PetscNewLog(ksp,&cg);CHKERRQ(ierr);
#if !defined(PETSC_USE_COMPLEX)
  cg->type = KSP_CG_SYMMETRIC;
#else
  cg->type = KSP_CG_HERMITIAN;
#endif
  ksp->data = (void*)cg;

  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_PRECONDITIONED,PC_LEFT,3);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_UNPRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_NATURAL,PC_LEFT,2);CHKERRQ(ierr);

  /*
       Sets the functions that are associated with this data structure
       (in C++ this is the same as defining virtual functions)
  */
  ksp->ops->setup          = KSPSetUp_SACG;
  ksp->ops->solve          = KSPSolve_SACG;
  ksp->ops->destroy        = KSPDestroy_SACG;
  ksp->ops->view           = KSPView_SACG;
  ksp->ops->setfromoptions = KSPSetFromOptions_SACG;
  ksp->ops->buildsolution  = KSPBuildSolutionDefault;
  ksp->ops->buildresidual  = KSPBuildResidualDefault;

  /*
      Attach the function KSPCGSetType_SACG() to this object. The routine
      KSPCGSetType() checks for this attached function and calls it if it finds
      it. (Sort of like a dynamic member function that can be added at run time
  */
  ierr = PetscObjectComposeFunction((PetscObject)ksp,"KSPCGSetType_C",
                                    KSPCGSetType_SACG);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)ksp,"KSPCGUseSingleReduction_C",
                                    KSPCGUseSingleReduction_SACG);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
