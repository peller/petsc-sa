#include <petsc/private/kspimpl.h>
#include <petscnb.h>
#include <sa_app.h>

/*
 KSPSetUp_SANBPCG - Sets up the workspace needed by the SANBPCG method.

 This is called once, usually automatically by KSPSolve() or KSPSetUp()
 but can be called directly by KSPSetUp()
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSetUp_SANBPCG"
static PetscErrorCode KSPSetUp_SANBPCG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPSetWorkVecs(ksp,6);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 KSPSolve_SANBPCG

 Input Parameter:
 .     ksp - the Krylov space object that was set to use conjugate gradient, by, for
             example, KSPCreate(MPI_Comm,KSP *ksp); KSPSetType(ksp,KSPCG);
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSolve_SANBPCG"
static PetscErrorCode  KSPSolve_SANBPCG(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i,n;
  PetscScalar    alpha=0.0,beta=0.0,gamma,gammaNew,delta,mpivals[2];
  PetscReal      dp=0.0;
  Vec            x,b,r,p,s,S,z,Z;
  Mat            Amat,Pmat;
  PetscBool      diagonalscale;
  MPI_Comm       pcomm;
  MPI_Request    req;
  MPI_Status     stat;

  PetscFunctionBegin;
  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);
  pcomm = PetscObjectComm((PetscObject)ksp);
  ierr = PCGetDiagonalScale(ksp->pc,&diagonalscale);CHKERRQ(ierr);
  if (diagonalscale) SETERRQ1(pcomm,PETSC_ERR_SUP,"Krylov method %s does not support diagonal scaling",
                              ((PetscObject)ksp)->type_name);

  // Check if normtype is supported
  if(ksp->normtype!=KSP_NORM_PRECONDITIONED && ksp->normtype!=KSP_NORM_UNPRECONDITIONED &&
     ksp->normtype!=KSP_NORM_NATURAL && ksp->normtype!=KSP_NORM_NONE) {
    SETERRQ1(pcomm,PETSC_ERR_SUP,"%s",KSPNormTypes[ksp->normtype]);
  }

  x = ksp->vec_sol; b = ksp->vec_rhs;
  r = ksp->work[0]; p = ksp->work[1];
  s = ksp->work[2]; S = ksp->work[3];
  z = ksp->work[4]; Z = ksp->work[5];

  ierr = PCGetOperators(ksp->pc,&Amat,&Pmat);CHKERRQ(ierr);

  // Initialize MPI_Test non-blocking allreduce
  ierr = VecGetLocalSize(b,&n);CHKERRQ(ierr);
  ierr = NBCreate(n,1);CHKERRQ(ierr);

  ksp->its = 0;
  if (!ksp->guess_zero) {
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,x,r),NTD_MV);CHKERRQ(ierr); // r <- b - Ax
    NT_ITER_FUNC(ierr = VecAYPX(r,-1.0,b),NTD_VECOPSMV);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = VecCopy(b,r),NTD_VECOPSMV);CHKERRQ(ierr);         // r <- b (x is 0)
  }

  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,r,z),NTD_PC);CHKERRQ(ierr);        // z <- Br

  /***********************************************************
   * Compute:
   * gamma = r'*z
   * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
   *      or sqrt(gamma) (Natural)
   **********************************************************/
  NT_ITER_FUNC(ierr = VecMergedDot_SANBPCG(r,z,ksp->normtype,&gamma,&dp),NTD_VECOPSMV);CHKERRQ(ierr);

  // Start Iallreduce
  mpivals[0] = gamma; mpivals[1] = dp;
  NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,MPI_SUM,pcomm,&req),NTD_MVIALLR);CHKERRQ(ierr);
  ierr     = NBInit(&req,&stat);CHKERRQ(ierr);

  // Compute Matvec
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,z,Z),NTD_MV);CHKERRQ(ierr); // Z <- Az

  // Finish Iallreduce
  NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_MVWAIT);CHKERRQ(ierr);
  gamma = mpivals[0];
  dp    = PetscSqrtReal(PetscAbsScalar(mpivals[1]));
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVIALLR)+NT_ITER_TIME(NTD_MVTEST)+
              NT_ITER_TIME(NTD_MVWAIT),NTD_MVALLR);

  // Set convergence information
  ierr       = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr       = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ksp->rnorm = dp;
  ierr       = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

  // Set comm timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
              NT_ITER_TIME(NTD_MVALLR)+NT_ITER_TIME(NTD_PCALLR),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_PCALLR),NTD_ALLR);

  // Set comp timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
              NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPSMV),NTD_COMP);
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSMV),NTD_VECOPS);

  // Set allreduce comp timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSMV)+NT_ITER_TIME(NTD_MV_DIAG)+
              NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC),NTD_MVACOMP);

  // Set total timer
  NT_ITER_END(NTD_TOTAL);
  NT_ITER_FINISH();

  if (ksp->reason) PetscFunctionReturn(0);

  i = 0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);
    /********************************************
     * Compute:
     * x = x + a*p    p = z + b*p
     * s = Z + b*s    delta = p'*s
     *******************************************/
    NT_ITER_FUNC(ierr = VecMergedOps1_SANBPCG(x,p,z,s,Z,alpha,beta,&delta),NTD_VECOPSPC);CHKERRQ(ierr);

    // Start Iallreduce
    mpivals[0] = delta;
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,1,MPI_DOUBLE,MPI_SUM,pcomm,&req),NTD_PCIALLR);
    ierr = NBInit(&req,&stat);CHKERRQ(ierr);

    // Compute PC
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,s,S),NTD_PC);CHKERRQ(ierr); // S <- Bs

    // Finish Iallreduce
    NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_PCWAIT);CHKERRQ(ierr);
    delta = mpivals[0];
    NT_ITER_ADD(NT_ITER_TIME(NTD_PCIALLR)+NT_ITER_TIME(NTD_PCTEST)+
                NT_ITER_TIME(NTD_PCWAIT),NTD_PCALLR);

    alpha = gamma / delta;

    /************************************************************
     * Compute:
     * r = r - a*s    z = z - a*S
     * gamma = r'*z
     * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
     *      or sqrt(gamma) (Natural)
     ***********************************************************/
    NT_ITER_FUNC(ierr = VecMergedOps2_SANBPCG(r,s,z,S,ksp->normtype,alpha,&gammaNew,&dp),NTD_VECOPSMV);CHKERRQ(ierr);

    // Start Iallreduce
    mpivals[0] = gammaNew; mpivals[1] = dp;
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,MPI_SUM,pcomm,&req),NTD_MVIALLR);CHKERRQ(ierr);
    ierr = NBInit(&req,&stat);CHKERRQ(ierr);

    // Compute Matvec
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,z,Z),NTD_MV);CHKERRQ(ierr); // Z <- Az

    // Finish Iallreduce
    NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_MVWAIT);CHKERRQ(ierr);
    gammaNew = mpivals[0];
    dp       = PetscSqrtReal(PetscAbsScalar(mpivals[1]));
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVIALLR)+NT_ITER_TIME(NTD_MVTEST)+
                NT_ITER_TIME(NTD_MVWAIT),NTD_MVALLR);

    // Set convergence information
    i++;
    ksp->its = i;
    ksp->rnorm = dp;
    ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    ierr = KSPMonitor(ksp,i,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

    // Compute final x vector
    if (ksp->reason || i>=ksp->max_it) {
      ierr = VecAXPY(x,alpha,p);CHKERRQ(ierr); // x <- x + alpha * p
    }

    // Set comm timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
                NT_ITER_TIME(NTD_MVALLR)+NT_ITER_TIME(NTD_PCALLR),NTD_COMM);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVALLR)+NT_ITER_TIME(NTD_PCALLR),NTD_ALLR);

    // Set comp timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC)+
                NT_ITER_TIME(NTD_VECOPSPC)+NT_ITER_TIME(NTD_VECOPSMV),NTD_COMP);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSPC)+NT_ITER_TIME(NTD_VECOPSMV),NTD_VECOPS);

    // Set allreduce comp timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSMV)+NT_ITER_TIME(NTD_MV_DIAG)+
                NT_ITER_TIME(NTD_MV_OFFD),NTD_MVACOMP);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPSPC)+NT_ITER_TIME(NTD_PC),NTD_PCACOMP);

    // Set total timer
    NT_ITER_END(NTD_TOTAL);
    NT_ITER_FINISH();

    if (ksp->reason) break;

    beta  = gammaNew / gamma;
    gamma = gammaNew;

  } while (i<ksp->max_it);
  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;

  // Destroy MPI_Test non-blocking allreduce
  ierr = NBDestroy();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*MC
   KSPSANBPCG - A pipelined conjugate gradient method from Bill Gropp

   This method has two reductions, one of which is overlapped with the matrix-vector product and one of which is
   overlapped with the preconditioner.

   This implementation uses optimized vector operations and reductions for
   improved performance. Contains most of the functionality from standard
   PETSc GROPPCG implementation, but may not have some features.
   Reference: Eller and Gropp, 2016.

   See nb_utils.c for details on how to use non-blocking allreduce with
   MPI_Test() with the routines NBCreate, NBInit, NBDestroy,
   NBTestMV and NBTestPC.

   Options Database Keys:
+  -use_nb <use_nb>     - Use non-blocking allreduce with MPI_Test()
.  -nb_mv_mult <mvmult> - Set multiplier for how often to call MPI_Test() in
                          matrix-vector multiply. MPI_Test() is called
                          mvmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.
-  -nb_pc_mult <pcmult> - Set multiplier for how often to call MPI_Test() in
                          preconditioner application. MPI_Test() is called
                          pcmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.

   Level: intermediate

   Notes:
   MPI configuration may be necessary for reductions to make asynchronous progress, which is important for performance of pipelined methods.
   See the FAQ on the PETSc website for details.

   Original method contributed by:
   Pieter Ghysels, Universiteit Antwerpen, Intel Exascience lab Flanders

   Reference:
   http://www.cs.uiuc.edu/~wgropp/bib/talks/tdata/2012/icerm.pdf

.seealso: KSPCreate(), KSPSetType(), KSPPIPECG, KSPPIPECR, KSPPGMRES, KSPCG, KSPCGUseSingleReduction()
M*/

#undef __FUNCT__
#define __FUNCT__ "KSPCreate_SANBPCG"
PETSC_EXTERN PetscErrorCode KSPCreate_SANBPCG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_UNPRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_PRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_NATURAL,PC_LEFT,2);CHKERRQ(ierr);

  ksp->ops->setup          = KSPSetUp_SANBPCG;
  ksp->ops->solve          = KSPSolve_SANBPCG;
  ksp->ops->destroy        = KSPDestroyDefault;
  ksp->ops->view           = 0;
  ksp->ops->setfromoptions = 0;
  ksp->ops->buildsolution  = KSPBuildSolutionDefault;
  ksp->ops->buildresidual  = KSPBuildResidualDefault;
  PetscFunctionReturn(0);
}
