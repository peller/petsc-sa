#include <petsc/private/kspimpl.h>
#include <petscnb.h>
#include <sa_app.h>

/*
     KSPSetUp_SAPIPECG - Sets up the workspace needed by the SAPIPECG method.

      This is called once, usually automatically by KSPSolve() or KSPSetUp()
     but can be called directly by KSPSetUp()
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSetUp_SAPIPECG"
static PetscErrorCode KSPSetUp_SAPIPECG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* get work vectors needed by SAPIPECG */
  ierr = KSPSetWorkVecs(ksp,9);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 KSPSolve_SAPIPECG - This routine actually applies the pipelined conjugate gradient method

 Input Parameter:
 .     ksp - the Krylov space object that was set to use conjugate gradient, by, for
             example, KSPCreate(MPI_Comm,KSP *ksp); KSPSetType(ksp,KSPCG);
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSolve_SAPIPECG"
static PetscErrorCode  KSPSolve_SAPIPECG(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i,n,allr_type=0;
  PetscScalar    alpha=0.0,beta=0.0,gamma=0.0,gammaold=0.0,delta=0.0,mpivals[3];
  PetscReal      dp=0.0;
  Vec            X,B,Z,P,W,Q,U,M,N,R,S;
  Mat            Amat,Pmat;
  PetscBool      diagonalscale, set;
  MPI_Comm       pcomm;
  MPI_Request    req,*reqs;
  MPI_Status     stat,*stats;

  PetscFunctionBegin;
  // Get allr_type option
  ierr = PetscOptionsGetInt(NULL,NULL,"-allr_type",&allr_type,&set);CHKERRQ(ierr);
  ierr = PetscMalloc(2*sizeof(MPI_Request),&reqs);CHKERRQ(ierr);
  ierr = PetscMalloc(2*sizeof(MPI_Status),&stats);CHKERRQ(ierr);

  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);
  pcomm = PetscObjectComm((PetscObject)ksp);
  ierr = PCGetDiagonalScale(ksp->pc,&diagonalscale);CHKERRQ(ierr);
  if (diagonalscale) SETERRQ1(pcomm,PETSC_ERR_SUP,"Krylov method %s does not support diagonal scaling",((PetscObject)ksp)->type_name);

  // Check if normtype is supported
  if(ksp->normtype!=KSP_NORM_PRECONDITIONED && ksp->normtype!=KSP_NORM_UNPRECONDITIONED &&
     ksp->normtype!=KSP_NORM_NATURAL && ksp->normtype!=KSP_NORM_NONE) {
    SETERRQ1(pcomm,PETSC_ERR_SUP,"%s",KSPNormTypes[ksp->normtype]);
  }

  X = ksp->vec_sol; B = ksp->vec_rhs; M = ksp->work[0]; Z = ksp->work[1];
  P = ksp->work[2]; N = ksp->work[3]; W = ksp->work[4]; Q = ksp->work[5];
  U = ksp->work[6]; R = ksp->work[7]; S = ksp->work[8];
  ierr = PCGetOperators(ksp->pc,&Amat,&Pmat);CHKERRQ(ierr);

  // Initialize MPI_Test non-blocking allreduce
  ierr = VecGetLocalSize(B,&n);CHKERRQ(ierr);
  ierr = NBCreate(n,2);CHKERRQ(ierr);

  ksp->its = 0;
  if (!ksp->guess_zero) {
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,X,R),NTD_MV);CHKERRQ(ierr); // r <- b - Ax
    NT_ITER_FUNC(ierr = VecAYPX(R,-1.0,B),NTD_VECOPS);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = VecCopy(B,R),NTD_VECOPS);CHKERRQ(ierr);         // r <- b (x is 0)
  }

  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R,U),NTD_PC);CHKERRQ(ierr);        // u <- Br
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,U,W),NTD_MV);CHKERRQ(ierr);   // w <- Au

  /************************************************************
   * Compute merged vector operations for SAPIPECG:
   * gamma = r'*u
   * delta = w'*u
   * dp = ||u|| (Preconditioned) or ||r|| (Unpreconditioned)
   *      or sqrt(gamma) (Natural)
   ***********************************************************/
  NT_ITER_FUNC(ierr = VecMergedDot_SAPIPECG(R,U,W,ksp->normtype,&gamma,&delta,&dp),NTD_VECOPS);CHKERRQ(ierr);

  // Start Iallreduce
  mpivals[0] = gamma; mpivals[1] = delta; mpivals[2] = dp;
  if(allr_type==1) {
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[0]),NTD_MVIALLR);CHKERRQ(ierr);
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,&mpivals[2],1,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[1]),NTD_MVIALLR);CHKERRQ(ierr);
    ierr = NBInitAll(&reqs,&stats,2);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,3,MPI_DOUBLE,MPI_SUM,pcomm,&req),NTD_MVIALLR);CHKERRQ(ierr);
    ierr = NBInit(&req,&stat);CHKERRQ(ierr);
  }

  // Compute PC and Matvec
  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,W,M),NTD_PC);CHKERRQ(ierr);      // m <- Bw
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,M,N),NTD_MV);CHKERRQ(ierr); // n <- Am

  // Finish Iallreduce
  if(allr_type==1) {
    NT_ITER_FUNC(ierr = MPI_Waitall(2,reqs,stats),NTD_MVWAIT);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_MVWAIT);CHKERRQ(ierr);
  }
  gamma = mpivals[0]; delta = mpivals[1];
  dp    = PetscSqrtReal(PetscAbsScalar(mpivals[2]));
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVIALLR)+NT_ITER_TIME(NTD_MVTEST)+
              NT_ITER_TIME(NTD_PCTEST)+NT_ITER_TIME(NTD_MVWAIT),NTD_ALLR);

  // Set convergence info
  ksp->rnorm = dp;
  ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ierr = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

  // Set comm and total timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
              NT_ITER_TIME(NTD_ALLR),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
              NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPS),NTD_COMP);
  NT_ITER_END(NTD_TOTAL);
  NT_ITER_FINISH();

  if (ksp->reason) PetscFunctionReturn(0);

  i = 0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);

    // Compute variables
    if (i == 0) {
      alpha = gamma / delta;
    } else {
      beta  = gamma / gammaold;
      alpha = gamma / (delta - beta / alpha * gamma);
    }
    gammaold = gamma;

    /************************************************************
     * Compute:
     * z = n + b*z   q = m + b*q
     * p = u + b*p   s = w + b*s
     * x = x + a*p   u = u - a*q
     * w = w - a*z   r = r - a*s
     * gamma = r'*u  delta = w'*u
     * dp = ||u|| (Preconditioned) or ||r|| (Unpreconditioned)
     *      or sqrt(gamma) (Natural)
     ***********************************************************/
    NT_ITER_FUNC(ierr = VecMergedOps_SAPIPECG(Z,Q,P,S,X,U,W,R,N,M,ksp->normtype,beta,
                                              alpha,&gamma,&delta,&dp),NTD_VECOPS);CHKERRQ(ierr);

    // Start Iallreduce
    mpivals[0] = gamma; mpivals[1] = delta; mpivals[2] = dp;
    if(allr_type==1){
      NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[0]),NTD_MVIALLR);CHKERRQ(ierr);
      NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,&mpivals[2],1,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[1]),NTD_MVIALLR);CHKERRQ(ierr);
      ierr = NBInitAll(&reqs,&stats,2);CHKERRQ(ierr);
    } else {
      NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,3,MPI_DOUBLE,MPI_SUM,pcomm,&req),NTD_MVIALLR);CHKERRQ(ierr);
      ierr = NBInit(&req,&stat);CHKERRQ(ierr);
    }

    // Compute PCs and Matvecs
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,W,M),NTD_PC);CHKERRQ(ierr);      // m <- Bw
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,M,N),NTD_MV);CHKERRQ(ierr); // n <- Am

    // Finish Iallreduce
    if(allr_type==1) {
      NT_ITER_FUNC(ierr = MPI_Waitall(2,reqs,stats),NTD_MVWAIT);CHKERRQ(ierr);
    } else {
      NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_MVWAIT);CHKERRQ(ierr);
    }
    gamma = mpivals[0]; delta = mpivals[1];
    dp    = PetscSqrtReal(PetscAbsScalar(mpivals[2]));
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVIALLR)+NT_ITER_TIME(NTD_MVTEST)+
                NT_ITER_TIME(NTD_PCTEST)+NT_ITER_TIME(NTD_MVWAIT),NTD_ALLR);

    // Set convergence info
    i++;
    ksp->its = i;
    ksp->rnorm = dp;
    ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    ierr = KSPMonitor(ksp,i,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

    // Set comm and total timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
                NT_ITER_TIME(NTD_ALLR),NTD_COMM);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
                NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPS),NTD_COMP);
    NT_ITER_END(NTD_TOTAL);
    NT_ITER_FINISH();

    if (ksp->reason) break;

  } while (i<ksp->max_it);
  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;

  // Destroy MPI_Test non-blocking allreduce
  ierr = NBDestroy();CHKERRQ(ierr);
  ierr = PetscFree(reqs);CHKERRQ(ierr);
  ierr = PetscFree(stats);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/*MC
   KSPSAPIPECG - Pipelined conjugate gradient method.

   This method has only a single non-blocking reduction per iteration, compared to 2 blocking for standard CG.  The
   non-blocking reduction is overlapped by the matrix-vector product and preconditioner application.4

   This implementation uses optimized vector operations and reductions for
   improved performance. Contains most of the functionality from standard
   PETSc CG implementation, but may not have some features.
   Reference: Eller and Gropp, 2016.

   See nb_utils.c for details on how to use non-blocking allreduce with
   MPI_Test() with the routines NBCreate, NBInit, NBDestroy,
   NBTestMV and NBTestPC.

   Options Database Keys:
+  -use_nb <use_nb>     - Use non-blocking allreduce with MPI_Test()
.  -nb_mv_mult <mvmult> - Set multiplier for how often to call MPI_Test() in
                          matrix-vector multiply. MPI_Test() is called
                          mvmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.
-  -nb_pc_mult <pcmult> - Set multiplier for how often to call MPI_Test() in
                          preconditioner application. MPI_Test() is called
                          pcmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.

   Level: intermediate

   Notes:
   MPI configuration may be necessary for reductions to make asynchronous progress, which is important for performance of pipelined methods.
   See the FAQ on the PETSc website for details.

   Original method contributed by:
   Pieter Ghysels, Universiteit Antwerpen, Intel Exascience lab Flanders

   Reference:
   P. Ghysels and W. Vanroose, "Hiding global synchronization latency in the preconditioned Conjugate Gradient algorithm",
   Submitted to Parallel Computing, 2012.

.seealso: KSPCreate(), KSPSetType(), KSPPIPECR, KSPGROPPCG, KSPPGMRES, KSPCG, KSPCGUseSingleReduction()
M*/
#undef __FUNCT__
#define __FUNCT__ "KSPCreate_SAPIPECG"
PETSC_EXTERN PetscErrorCode KSPCreate_SAPIPECG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_UNPRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_PRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_NATURAL,PC_LEFT,2);CHKERRQ(ierr);

  ksp->ops->setup          = KSPSetUp_SAPIPECG;
  ksp->ops->solve          = KSPSolve_SAPIPECG;
  ksp->ops->destroy        = KSPDestroyDefault;
  ksp->ops->view           = 0;
  ksp->ops->setfromoptions = 0;
  ksp->ops->buildsolution  = KSPBuildSolutionDefault;
  ksp->ops->buildresidual  = KSPBuildResidualDefault;
  PetscFunctionReturn(0);
}
