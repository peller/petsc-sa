#include <petsc/private/kspimpl.h>
#include <petscnb.h>
#include <sa_app.h>

/*
     KSPSetUp_SAPIPE2CG - Sets up the workspace needed by the SAPIPE2CG method.

      This is called once, usually automatically by KSPSolve() or KSPSetUp()
     but can be called directly by KSPSetUp()
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSetUp_SAPIPE2CG"
static PetscErrorCode KSPSetUp_SAPIPE2CG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* get work vectors needed by SAPIPE2CG */
  ierr = KSPSetWorkVecs(ksp,25);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 KSPSolve_SAPIPE2CG - This routine actually applies the pipelined conjugate gradient method

 Input Parameter:
 .     ksp - the Krylov space object that was set to use conjugate gradient, by, for
             example, KSPCreate(MPI_Comm,KSP *ksp); KSPSetType(ksp,KSPCG);
*/
#undef __FUNCT__
#define __FUNCT__ "KSPSolve_SAPIPE2CG"
static PetscErrorCode  KSPSolve_SAPIPE2CG(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i, n, allr_type;
  PetscScalar    beta[2],gamma[2],rho[2],delta=0.0,lambda[10],phi[3];
  PetscReal      dp=0.0;
  Vec            B,X[3],R[3],Z[3],W[3],P[3],Q[3],C[3],D[3],G,H;
  Mat            Amat,Pmat;
  PetscBool      diagonalscale;
  MPI_Comm       pcomm;
  MPI_Request    req,*reqs;
  MPI_Status     stat,*stats;

  PetscFunctionBegin;
  // Get allr_type option
  allr_type = 0;
  ierr = PetscOptionsGetInt(NULL,NULL,"-allr_type",&allr_type,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc(5*sizeof(MPI_Request),&reqs);CHKERRQ(ierr);
  ierr = PetscMalloc(5*sizeof(MPI_Status),&stats);CHKERRQ(ierr);

  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);
  pcomm = PetscObjectComm((PetscObject)ksp);
  ierr = PCGetDiagonalScale(ksp->pc,&diagonalscale);CHKERRQ(ierr);
  if (diagonalscale) SETERRQ1(pcomm,PETSC_ERR_SUP,"Krylov method %s does not support diagonal scaling",((PetscObject)ksp)->type_name);

  // Check if normtype is supported
  if(ksp->normtype!=KSP_NORM_PRECONDITIONED && ksp->normtype!=KSP_NORM_UNPRECONDITIONED &&
     ksp->normtype!=KSP_NORM_NATURAL && ksp->normtype!=KSP_NORM_NONE) {
    SETERRQ1(pcomm,PETSC_ERR_SUP,"%s",KSPNormTypes[ksp->normtype]);
  }

  B    = ksp->vec_rhs;
  X[0] = ksp->vec_sol;  X[1] = ksp->work[0];  X[2] = ksp->work[1];
  R[0] = ksp->work[2];  R[1] = ksp->work[3];  R[2] = ksp->work[4];
  Z[0] = ksp->work[5];  Z[1] = ksp->work[6];  Z[2] = ksp->work[7];
  W[0] = ksp->work[8];  W[1] = ksp->work[9];  W[2] = ksp->work[10];
  P[0] = ksp->work[11]; P[1] = ksp->work[12]; P[2] = ksp->work[13];
  Q[0] = ksp->work[14]; Q[1] = ksp->work[15]; Q[2] = ksp->work[16];
  C[0] = ksp->work[17]; C[1] = ksp->work[18]; C[2] = ksp->work[19];
  D[0] = ksp->work[20]; D[1] = ksp->work[21]; D[2] = ksp->work[22];
  G    = ksp->work[23]; H    = ksp->work[24];
  ierr = PCGetOperators(ksp->pc,&Amat,&Pmat);CHKERRQ(ierr);

  PetscMemzero(gamma,2*sizeof(PetscScalar));
  PetscMemzero(rho,2*sizeof(PetscScalar));
  PetscMemzero(beta,2*sizeof(PetscScalar));

  // Initialize MPI_Test non-blocking allreduce
  ierr = VecGetLocalSize(B,&n);CHKERRQ(ierr);
  ierr = NBCreate(n,4);CHKERRQ(ierr);

  ksp->its = 0;
  if (!ksp->guess_zero) {
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,X[0],R[0]),NTD_MV);CHKERRQ(ierr); // r <- b - Ax
    NT_ITER_FUNC(ierr = VecAYPX(R[0],-1.0,B),NTD_VECOPS);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = VecCopy(B,R[0]),NTD_VECOPS);CHKERRQ(ierr);            // r <- b (x is 0)
  }

  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R[0],Z[0]),NTD_PC);CHKERRQ(ierr);        // z <- Mr
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,Z[0],W[0]),NTD_MV);CHKERRQ(ierr);   // w <- Az

  /****************************************
   * Compute merged vector operations:
   * delta = z*w    beta  = z*r
   * dp    = z*z
   ***************************************/
  NT_ITER_FUNC(ierr = VecMergedDot_SAPIPE2CG(Z[0],W[0],R[0],ksp->normtype,&delta,
                                             &(beta[0]),&dp),NTD_VECOPS);CHKERRQ(ierr);

  // Start Iallreduce
  lambda[0] = delta; lambda[1] = beta[0]; lambda[2] = dp;
  if(allr_type==1) {
    NT_ITER_START(NTD_MVIALLR);
    ierr = MPI_Iallreduce(MPI_IN_PLACE,lambda,2,MPI_DOUBLE,MPI_SUM,
                          pcomm,&reqs[0]);CHKERRQ(ierr);
    ierr = MPI_Iallreduce(MPI_IN_PLACE,&lambda[2],1,MPI_DOUBLE,MPI_SUM,
                          pcomm,&reqs[1]);CHKERRQ(ierr);
    NT_ITER_END(NTD_MVIALLR);
    ierr = NBInitAll(&reqs,&stats,2);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,lambda,3,MPI_DOUBLE,MPI_SUM,
                                       pcomm,&req),NTD_MVIALLR);CHKERRQ(ierr);
    ierr = NBInit(&req,&stat);CHKERRQ(ierr);
  }

  // Compute PCs and Matvecs
  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,W[0],P[0]),NTD_PC1);CHKERRQ(ierr);      // p <- Mw
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,P[0],Q[0]),NTD_MV1);CHKERRQ(ierr); // q <- Ap
  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,Q[0],C[0]),NTD_PC2);CHKERRQ(ierr);      // c <- Mq
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,C[0],D[0]),NTD_MV2);CHKERRQ(ierr); // d <- Ac
  NT_ITER_ADD(NT_ITER_TIME(NTD_PC1)+NT_ITER_TIME(NTD_PC2),NTD_PC);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV1)+NT_ITER_TIME(NTD_MV2),NTD_MV);

  // Finish Iallreduce
  if(allr_type==1) {
    NT_ITER_FUNC(ierr = MPI_Waitall(2,reqs,stats),NTD_MVWAIT);CHKERRQ(ierr);
  } else {
    NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_MVWAIT);CHKERRQ(ierr);
  }
  delta = lambda[0]; beta[0] = lambda[1];
  dp    = PetscSqrtReal(PetscAbsScalar(lambda[2]));
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVIALLR)+NT_ITER_TIME(NTD_MVTEST)+
              NT_ITER_TIME(NTD_PCTEST)+NT_ITER_TIME(NTD_MVWAIT),NTD_ALLR);

  // Set convergence info
  ksp->rnorm = dp;
  ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ierr = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

  // Set comm and total timers
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
              NT_ITER_TIME(NTD_ALLR),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
              NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPS),NTD_COMP);
  NT_ITER_END(NTD_TOTAL);
  NT_ITER_FINISH();

  if (ksp->reason) PetscFunctionReturn(0);

  i = 0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);

    // Compute variables
    if (i == 0) {
      rho[0]   = 1.0;
      gamma[1] = gamma[0];
      gamma[0] = beta[0]/delta;
    } else {
      gamma[1] = gamma[0];
      gamma[0] = beta[0]/delta;
      rho[1]   = rho[0];
      rho[0]   = 1.0/(1.0 - (gamma[0]/gamma[1]) * (beta[0]/beta[1]) * (1.0/rho[1]));

      phi[0] = rho[0];
      phi[1] = -rho[0]*gamma[0];
      phi[2] = 1.0-rho[0];
      delta  = phi[0]*phi[0]*lambda[0] + 2.0*phi[0]*phi[1]*lambda[1] + 2.0*phi[0]*phi[2]*lambda[2]
             + phi[1]*phi[1]*lambda[3] + 2.0*phi[1]*phi[2]*lambda[4] + phi[2]*phi[2]*lambda[5];
      beta[1] = beta[0];
      beta[0] = phi[0]*phi[0]*lambda[6] + 2.0*phi[0]*phi[1]*lambda[0] + 2.0*phi[0]*phi[2]*lambda[7]
              + phi[1]*phi[1]*lambda[1] + 2.0*phi[1]*phi[2]*lambda[2] + phi[2]*phi[2]*lambda[8];

      gamma[1] = gamma[0];
      gamma[0] = beta[0]/delta;
      rho[1]   = rho[0];
      rho[0]   = 1.0/(1.0 - (gamma[0]/gamma[1]) * (beta[0]/beta[1]) * (1.0/rho[1]));
    }

    // Compute merged vector operations
    if(i==0) {
      NT_ITER_FUNC(ierr = VecMergedOpsShort_SAPIPE2CG(X,R,Z,W,P,Q,C,D,ksp->normtype,
                                                      rho,gamma,lambda),NTD_VECOPS);CHKERRQ(ierr);
    } else {
      NT_ITER_FUNC(ierr = VecMergedOps_SAPIPE2CG(X,R,Z,W,P,Q,C,D,G,H,ksp->normtype,
                                                 rho,gamma,lambda),NTD_VECOPS);CHKERRQ(ierr);
    }

    // Start Iallreduce
    if(allr_type==1) {
      NT_ITER_START(NTD_MVIALLR);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,lambda,2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[0]);CHKERRQ(ierr);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,&lambda[2],2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[1]);CHKERRQ(ierr);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,&lambda[4],2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[2]);CHKERRQ(ierr);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,&lambda[6],2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[3]);CHKERRQ(ierr);
      ierr = MPI_Iallreduce(MPI_IN_PLACE,&lambda[8],2,MPI_DOUBLE,MPI_SUM,pcomm,&reqs[4]);CHKERRQ(ierr);
      NT_ITER_END(NTD_MVIALLR);
      ierr = NBInitAll(&reqs,&stats,5);CHKERRQ(ierr);
    } else {
      NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,lambda,10,MPI_DOUBLE,MPI_SUM,pcomm,&req),NTD_MVIALLR);CHKERRQ(ierr);
      ierr = NBInit(&req,&stat);CHKERRQ(ierr);
    }

    // Compute PCs and Matvecs
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,Q[0],C[0]),NTD_PC1);CHKERRQ(ierr);      // c <- Mq
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,C[0],D[0]),NTD_MV1);CHKERRQ(ierr); // d <- Ac
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,D[0],G),NTD_PC2);CHKERRQ(ierr);         // g <- Md
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,Amat,G,H),NTD_MV2);CHKERRQ(ierr);       // h <- Ag
    NT_ITER_ADD(NT_ITER_TIME(NTD_PC1)+NT_ITER_TIME(NTD_PC2),NTD_PC);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV1)+NT_ITER_TIME(NTD_MV2),NTD_MV);

    // Finish Iallreduce
    if(allr_type==1) {
      NT_ITER_FUNC(ierr = MPI_Waitall(5,reqs,stats),NTD_MVWAIT);CHKERRQ(ierr);
    } else {
      NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_MVWAIT);CHKERRQ(ierr);
    }
    beta[1] = beta[0]; beta[0] = lambda[6]; delta = lambda[0];
    dp      = PetscSqrtReal(PetscAbsScalar(lambda[9]));
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVIALLR)+NT_ITER_TIME(NTD_MVTEST)+NT_ITER_TIME(NTD_PCTEST)
                +NT_ITER_TIME(NTD_MVWAIT),NTD_ALLR);

    // Set convergence info
    if(i==0) i++;
    else     i+=2;
    ksp->its = i;
    ksp->rnorm = dp;
    ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    ierr = KSPMonitor(ksp,i,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);

    // Set comm and total timers
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)+
                NT_ITER_TIME(NTD_ALLR),NTD_COMM);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+
                NT_ITER_TIME(NTD_PC)+NT_ITER_TIME(NTD_VECOPS),NTD_COMP);
    NT_ITER_END(NTD_TOTAL);
    NT_ITER_FINISH();

    if (ksp->reason) break;

  } while (i<ksp->max_it);
  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;

  // Destroy MPI_Test non-blocking allreduce
  ierr = NBDestroy();CHKERRQ(ierr);
  ierr = PetscFree(reqs);CHKERRQ(ierr);
  ierr = PetscFree(stats);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/*MC
   KSPSAPIPE2CG - Pipelined conjugate gradient method.

   This method combines two iterations of pipelined PCG into a single iteration with a single non-blocking reduction per iteration.
   The non-blocking reduction is overlapped by two matrix-vector products and two preconditioner applications.

   This implementation uses optimized vector operations and reductions for improved performance.
   Reference: Eller and Gropp, 2016.

   See nb_utils.c for details on how to use non-blocking allreduce with
   MPI_Test() with the routines NBCreate, NBInit, NBDestroy,
   NBTestMV and NBTestPC.

   Options Database Keys:
+  -use_nb <use_nb>     - Use non-blocking allreduce with MPI_Test()
.  -nb_mv_mult <mvmult> - Set multiplier for how often to call MPI_Test() in
                          matrix-vector multiply. MPI_Test() is called
                          mvmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.
-  -nb_pc_mult <pcmult> - Set multiplier for how often to call MPI_Test() in
                          preconditioner application. MPI_Test() is called
                          pcmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.

   See also KSPGROPPCG and KSPPIPECG, two other pipelined PCG methods.

   Level: intermediate

   Notes:
   MPI configuration may be necessary for reductions to make asynchronous progress, which is important for performance of pipelined methods.
   See the FAQ on the PETSc website for details.

.seealso: KSPCreate(), KSPSetType(), KSPPIPECR, KSPGROPPCG, KSPPGMRES, KSPCG, KSPCGUseSingleReduction()
M*/
#undef __FUNCT__
#define __FUNCT__ "KSPCreate_SAPIPE2CG"
PETSC_EXTERN PetscErrorCode KSPCreate_SAPIPE2CG(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_UNPRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_PRECONDITIONED,PC_LEFT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_NATURAL,PC_LEFT,2);CHKERRQ(ierr);

  ksp->ops->setup          = KSPSetUp_SAPIPE2CG;
  ksp->ops->solve          = KSPSolve_SAPIPE2CG;
  ksp->ops->destroy        = KSPDestroyDefault;
  ksp->ops->view           = 0;
  ksp->ops->setfromoptions = 0;
  ksp->ops->buildsolution  = KSPBuildSolutionDefault;
  ksp->ops->buildresidual  = KSPBuildResidualDefault;
  PetscFunctionReturn(0);
}
