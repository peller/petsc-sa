
/*
    This file implements pipelined BiCGStab (pipe-BiCGStab).
    Only allow right preconditioning.
*/
#include <../src/ksp/ksp/impls/sabcgs/sabcgsimpl.h>       /*I  "petscksp.h"  I*/
#include <petscnb.h>
#include <sa_app.h>

static PetscErrorCode KSPSetUp_SAPIPEBCGS(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPSetWorkVecs(ksp,15);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* Only need a few hacks from KSPSolve_BCGS */
#include <petsc/private/pcimpl.h>            /*I "petscksp.h" I*/
static PetscErrorCode  KSPSolve_SAPIPEBCGS(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i, n;
  PetscScalar    rho,rhoold,alpha,beta,omega,d1,d2,d3,mpivals[5];
  Vec            X,B,S,R,RP,Y,Q,P2,Q2,R2,S2,W,Z,W2,Z2,T,V;
  PetscReal      dp    = 0.0;
  KSP_SABCGS     *bcgs = (KSP_SABCGS*)ksp->data;
  PC             pc;
  MPI_Request    req;
  MPI_Status     stat;

  PetscFunctionBegin;
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  X  = ksp->vec_sol;
  B  = ksp->vec_rhs;
  R  = ksp->work[0];
  RP = ksp->work[1];
  S  = ksp->work[2];
  Y  = ksp->work[3];
  Q  = ksp->work[4];
  Q2 = ksp->work[5];
  P2 = ksp->work[6];
  R2 = ksp->work[7];
  S2 = ksp->work[8];
  W  = ksp->work[9];
  Z  = ksp->work[10];
  W2 = ksp->work[11];
  Z2 = ksp->work[12];
  T  = ksp->work[13];
  V  = ksp->work[14];

  // Initialize MPI_Test non-blocking allreduce
  ierr = VecGetLocalSize(B,&n);CHKERRQ(ierr);
  ierr = NBCreate(n,2);CHKERRQ(ierr);

  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);

  /* Only supports right preconditioning */
  NT_ITER_START(NTD_VECOPS);
  if (ksp->pc_side != PC_RIGHT) SETERRQ1(PetscObjectComm((PetscObject)ksp),PETSC_ERR_SUP,"KSP pipebcgs does not support %s",PCSides[ksp->pc_side]);
  if (!ksp->guess_zero) {
    if (!bcgs->guess) {
      ierr = VecDuplicate(X,&bcgs->guess);CHKERRQ(ierr);
    }
    ierr = VecCopy(X,bcgs->guess);CHKERRQ(ierr);
  } else {
    ierr = VecSet(X,0.0);CHKERRQ(ierr);
  }
  NT_ITER_END(NTD_VECOPS);

  /* Compute initial residual */
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  //ierr = PCSetUp(pc);CHKERRQ(ierr);
  if (!ksp->guess_zero) {
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,pc->mat,X,Q2),NTD_MV);CHKERRQ(ierr);
    NT_ITER_START(NTD_VECOPS);
    ierr = VecCopy(B,R);CHKERRQ(ierr);
    ierr = VecAXPY(R,-1.0,Q2);CHKERRQ(ierr);
    NT_ITER_END(NTD_VECOPS);
  } else {
    NT_ITER_FUNC(ierr = VecCopy(B,R),NTD_VECOPS);CHKERRQ(ierr);
  }

  /* Initialize */
  NT_ITER_FUNC(ierr = VecCopy(R,RP),NTD_VECOPS);CHKERRQ(ierr); /* rp <- r */

  // Start non-blocking allreduce
  NT_ITER_FUNC(ierr = VecDotBegin(R,RP,&rho),NTD_VECOPS);CHKERRQ(ierr); /* rho <- (r,rp) */
  NT_ITER_FUNC(ierr = PetscCommSplitReductionBegin(PetscObjectComm((PetscObject)R)),NTD_IALLR);CHKERRQ(ierr);

  // First overlapped MV+PC
  NT_ITER_START(NTD_MVPC1);
  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,R,R2),NTD_PC);CHKERRQ(ierr); /* r2 <- K r */
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,pc->mat,R2,W),NTD_MV);CHKERRQ(ierr); /* w <- A r2 */
  NT_ITER_END(NTD_MVPC1);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END),NTD_MVPCCOMM1);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC),NTD_MVPCCOMP1);

  // Finish non-blocking allreduce
  NT_ITER_FUNC(ierr = VecDotEnd(R,RP,&rho),NTD_WAIT);CHKERRQ(ierr);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVTEST),NTD_MVTEST1);
  NT_ITER_ADD(NT_ITER_TIME(NTD_PCTEST),NTD_PCTEST1);
  NT_ITER_ADD(NT_ITER_TIME(NTD_IALLR)+NT_ITER_TIME(NTD_MVTEST1)+NT_ITER_TIME(NTD_PCTEST1)
              +NT_ITER_TIME(NTD_WAIT),NTD_MVPCALLR1);
  dp = PetscSqrtReal(PetscAbsScalar(rho));

  // Check for convergence
  ierr       = PetscObjectSAWsTakeAccess((PetscObject)ksp);CHKERRQ(ierr);
  ksp->its   = 0;
  ksp->rnorm = dp;
  //if(rank==0) printf("pipebcgs iteration=%d norm=%e\n",ksp->its,ksp->rnorm);
  ierr = PetscObjectSAWsGrantAccess((PetscObject)ksp);CHKERRQ(ierr);
  ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ierr = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);
  if (ksp->reason) PetscFunctionReturn(0);

  // Start non-blocking allreduce
  NT_ITER_FUNC(ierr = VecDotBegin(W,RP,&d2),NTD_VECOPS);CHKERRQ(ierr); /* d2 <- (w,rp) */
  NT_ITER_FUNC(ierr = PetscCommSplitReductionBegin(PetscObjectComm((PetscObject)W)),NTD_IALLR2);CHKERRQ(ierr);

  // Second overlapped MV+PC
  NT_ITER_START(NTD_MVPC2);
  NT_ITER_FUNC(ierr = KSP_PCApply(ksp,W,W2),NTD_PC);CHKERRQ(ierr); /* w2 <- K w */
  NT_ITER_FUNC(ierr = KSP_MatMult(ksp,pc->mat,W2,T),NTD_MV);CHKERRQ(ierr); /* t <- A w2 */
  NT_ITER_END(NTD_MVPC2);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)
              -NT_ITER_TIME(NTD_MVPCCOMM1),NTD_MVPCCOMM2);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)
              +NT_ITER_TIME(NTD_PC)-NT_ITER_TIME(NTD_MVPCCOMP1),NTD_MVPCCOMP2);

  // Finish non-blocking allreduce
  NT_ITER_FUNC(ierr = VecDotEnd(W,RP,&d2),NTD_WAIT2);CHKERRQ(ierr);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVTEST)-NT_ITER_TIME(NTD_MVTEST1),NTD_MVTEST2);
  NT_ITER_ADD(NT_ITER_TIME(NTD_PCTEST)-NT_ITER_TIME(NTD_PCTEST1),NTD_PCTEST2);
  NT_ITER_ADD(NT_ITER_TIME(NTD_IALLR2)+NT_ITER_TIME(NTD_MVTEST2)+NT_ITER_TIME(NTD_PCTEST2)
              +NT_ITER_TIME(NTD_WAIT2),NTD_MVPCALLR2);

  alpha = rho/d2;
  beta  = 0.0;
  omega = 0.0;

  NT_ITER_END(NTD_TOTAL);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVPC1)+NT_ITER_TIME(NTD_MVPC2),NTD_MVPC);
  NT_ITER_SUB(NT_ITER_TIME(NTD_MVTEST),NTD_MV_DIAG);
  NT_ITER_SUB(NT_ITER_TIME(NTD_PCTEST),NTD_PC);
  NT_ITER_ADD(NT_ITER_TIME(NTD_MVPCALLR1)+NT_ITER_TIME(NTD_MVPCALLR2),NTD_ALLR);
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPS)+NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)
              +NT_ITER_TIME(NTD_PC),NTD_COMP);
  NT_ITER_ADD(NT_ITER_TIME(NTD_ALLR)+NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_TOTAL)-NT_ITER_TIME(NTD_COMM)-NT_ITER_TIME(NTD_COMP),NTD_EXTRA);
  NT_ITER_FINISH();

  /* Main loop */
  i=0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);

    /************************************************************
     * Merged Vector Operations:
     * p2 <- r2 - beta * omega * s2 + beta * p2
     * s  <- w  - beta * omega * z + beta * s
     * s2 <- w2 - beta * omega * z2 + beta * s2
     * z  <- t  - beta * omega * v + beta * z
     * q  <- r  - alpha s
     * q2 <- r2 - alpha s2
     * y  <- w  - alpha z
     * d1 <- (q,y)
     * d2 <- (y,y)
     ***********************************************************/
    NT_ITER_FUNC(ierr = VecMergedOps_PIPEBICGSTAB1(P2, Q, Q2, R, R2, S, S2, T, V, W,
                                                   W2, Y, Z, Z2, alpha, beta, omega,
                                                   i, &d1,&d2),NTD_VECOPS);CHKERRQ(ierr);

    // Start non-blocking allreduce
    mpivals[0] = d1; mpivals[1] = d2;
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,MPI_SUM,
                                       PetscObjectComm((PetscObject)Q),&req),NTD_IALLR);CHKERRQ(ierr);
    ierr = NBInit(&req,&stat);CHKERRQ(ierr);

    // First overlapped PC + MatVec
    NT_ITER_START(NTD_MVPC1);
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,Z,Z2),NTD_PC);CHKERRQ(ierr); /* z2 <- K z */
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,pc->mat,Z2,V),NTD_MV);CHKERRQ(ierr); /* v <- A z2 */
    NT_ITER_END(NTD_MVPC1);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END),NTD_MVPCCOMM1);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC),NTD_MVPCCOMP1);

    // Finish non-blocking allreduce
    NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_WAIT);CHKERRQ(ierr);
    d1 = mpivals[0]; d2 = mpivals[1];
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVTEST),NTD_MVTEST1);
    NT_ITER_ADD(NT_ITER_TIME(NTD_PCTEST),NTD_PCTEST1);
    NT_ITER_ADD(NT_ITER_TIME(NTD_IALLR)+NT_ITER_TIME(NTD_MVTEST1)+NT_ITER_TIME(NTD_PCTEST1)
                +NT_ITER_TIME(NTD_WAIT),NTD_MVPCALLR1);

    if (d2 == 0.0) {
      /* y is 0. if q is 0, then alpha s == r, and hence alpha p may be our solution. Give it a try? */
      NT_ITER_INDEX2(ierr = VecDot(Q,Q,&d1),NTD_MVPCALLR2,NTD_VECOPS);CHKERRQ(ierr);
      if (d1 != 0.0) {
        ksp->reason = KSP_DIVERGED_BREAKDOWN;
        break;
      }
      NT_ITER_FUNC(ierr = VecAXPY(X,alpha,P2),NTD_VECOPS);CHKERRQ(ierr);   /* x <- x + alpha p2 */
      ierr = PetscObjectSAWsTakeAccess((PetscObject)ksp);CHKERRQ(ierr);
      ksp->its++;
      ksp->rnorm  = 0.0;
      ksp->reason = KSP_CONVERGED_RTOL;
      ierr = PetscObjectSAWsGrantAccess((PetscObject)ksp);CHKERRQ(ierr);
      ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
      ierr = KSPMonitor(ksp,i+1,0.0);CHKERRQ(ierr);
      break;
    }

    omega  = d1/d2; /* omega <- (y'q) / (y'y) */
    rhoold = rho;

    /*************************************************
     * Merged Vector Operations:
     * x <- alpha * p2 + omega * q2 + x
     * r <- q - omega y
     * r2 <- w2 - alpha z2
     * r2 <- q2 - omega r2
     * w <- t - alpha v
     * w <- y - omega w
     * rho <- (r,rp)
     * d1 <- (s,rp)
     * d2 <- (w,rp)
     * d3 <- (z,rp)
     * dp <- (r,r)
     *************************************************/
    NT_ITER_FUNC(ierr = VecMergedOps_PIPEBICGSTAB2(P2, Q, Q2, R, R2, RP, S, T, V, W, W2,
                                                   X, Y, Z, Z2, alpha, omega, &rho, &d1,
                                                   &d2, &d3, &dp),NTD_VECOPS);CHKERRQ(ierr);

    // Start non-blocking allreduce
    mpivals[0] = rho; mpivals[1] = d1;
    mpivals[2] = d2;  mpivals[3] = d3;
    mpivals[4] = dp;
    NT_ITER_FUNC(ierr = MPI_Iallreduce(MPI_IN_PLACE,mpivals,5,MPI_DOUBLE,MPI_SUM,
                                       PetscObjectComm((PetscObject)R),&req),NTD_IALLR2);CHKERRQ(ierr);
    ierr = NBInit(&req,&stat);CHKERRQ(ierr);

    // Second overlapped PC + MatVec
    NT_ITER_START(NTD_MVPC2);
    NT_ITER_FUNC(ierr = KSP_PCApply(ksp,W,W2),NTD_PC);CHKERRQ(ierr); /* w2 <- K w */
    NT_ITER_FUNC(ierr = KSP_MatMult(ksp,pc->mat,W2,T),NTD_MV);CHKERRQ(ierr); /* t <- A w2 */
    NT_ITER_END(NTD_MVPC2);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)
                -NT_ITER_TIME(NTD_MVPCCOMM1),NTD_MVPCCOMM2);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)
                +NT_ITER_TIME(NTD_PC)-NT_ITER_TIME(NTD_MVPCCOMP1),NTD_MVPCCOMP2);

    // Finish non-blocking allreduce
    NT_ITER_FUNC(ierr = MPI_Wait(&req,&stat),NTD_WAIT2);CHKERRQ(ierr);
    rho = mpivals[0]; d1  = mpivals[1];
    d2  = mpivals[2]; d3  = mpivals[3];
    dp  = PetscSqrtReal(PetscAbsScalar(mpivals[4]));
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVTEST)-NT_ITER_TIME(NTD_MVTEST1),NTD_MVTEST2);
    NT_ITER_ADD(NT_ITER_TIME(NTD_PCTEST)-NT_ITER_TIME(NTD_PCTEST1),NTD_PCTEST2);
    NT_ITER_ADD(NT_ITER_TIME(NTD_IALLR2)+NT_ITER_TIME(NTD_MVTEST2)+NT_ITER_TIME(NTD_PCTEST2)
                +NT_ITER_TIME(NTD_WAIT2),NTD_MVPCALLR2);

    if (d2 + beta * d1 - beta * omega * d3 == 0.0)
      SETERRQ(PetscObjectComm((PetscObject)ksp),PETSC_ERR_PLIB,"Divide by zero");

    beta = (rho/rhoold) * (alpha/omega);
    alpha = rho/(d2 + beta * d1 - beta * omega * d3); /* alpha <- rho / (d2 + beta * d1 - beta * omega * d3) */

    ierr = PetscObjectSAWsTakeAccess((PetscObject)ksp);CHKERRQ(ierr);
    ksp->its++;

    /* Residual replacement step  */
    /*if (i > 0 && i%100 == 0 && i < 1001) {
      ierr = KSP_MatMult(ksp,pc->mat,X,R);CHKERRQ(ierr);
      ierr = VecAYPX(R,-1.0,B);CHKERRQ(ierr);              // r  <- b - Ax
      ierr = KSP_PCApply(ksp,R,R2);CHKERRQ(ierr);          // r2 <- K r
      ierr = KSP_MatMult(ksp,pc->mat,R2,W);CHKERRQ(ierr);  // w  <- A r2
      ierr = KSP_PCApply(ksp,W,W2);CHKERRQ(ierr);          // w2 <- K w
      ierr = KSP_MatMult(ksp,pc->mat,W2,T);CHKERRQ(ierr);  // t  <- A w2
      ierr = KSP_MatMult(ksp,pc->mat,P2,S);CHKERRQ(ierr);  // s  <- A p2
      ierr = KSP_PCApply(ksp,S,S2);CHKERRQ(ierr);          // s2 <- K s
      ierr = KSP_MatMult(ksp,pc->mat,S2,Z);CHKERRQ(ierr);  // z  <- A s2
      ierr = KSP_PCApply(ksp,Z,Z2);CHKERRQ(ierr);          // z2 <- K z
      ierr = KSP_MatMult(ksp,pc->mat,Z2,V);CHKERRQ(ierr);  // v  <- A z2
     }*/

    NT_ITER_END(NTD_TOTAL);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVPC1)+NT_ITER_TIME(NTD_MVPC2),NTD_MVPC);
    NT_ITER_SUB(NT_ITER_TIME(NTD_MVTEST),NTD_MV_DIAG);
    NT_ITER_SUB(NT_ITER_TIME(NTD_PCTEST),NTD_PC);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVPCALLR1)+NT_ITER_TIME(NTD_MVPCALLR2),NTD_ALLR);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPS)+NT_ITER_TIME(NTD_MV_DIAG)
                +NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC),NTD_COMP);
    NT_ITER_ADD(NT_ITER_TIME(NTD_ALLR)+NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END),NTD_COMM);
    NT_ITER_ADD(NT_ITER_TIME(NTD_TOTAL)-NT_ITER_TIME(NTD_COMM)-NT_ITER_TIME(NTD_COMP),NTD_EXTRA);
    NT_ITER_FINISH();

    ksp->rnorm = dp;
    //if(rank==0) printf("pipebcgs iteration=%d norm=%e\n",ksp->its,ksp->rnorm);
    ierr = PetscObjectSAWsGrantAccess((PetscObject)ksp);CHKERRQ(ierr);
    ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    ierr = KSPMonitor(ksp,i+1,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i+1,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);
    if (ksp->reason) break;
    if (rho == 0.0) {
      ksp->reason = KSP_DIVERGED_BREAKDOWN;
      break;
    }
    i++;
  } while (i<ksp->max_it);

  // Destroy MPI_Test non-blocking allreduce
  ierr = NBDestroy();CHKERRQ(ierr);

  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;
  PetscFunctionReturn(0);
}

/*MC
    KSPPIPEBCGS - Implements the pipelined BiCGStab method.

    This method has only two non-blocking reductions per iteration, compared to 3 blocking for standard FBCGS.  The
    non-blocking reductions are overlapped by matrix-vector products and preconditioner applications.

    Periodic residual replacement may be used to increase robustness and maximal attainable accuracy.

    Options Database Keys:
.see KSPSolve()

    Level: intermediate

    Notes:
    Like KSPFBCGS, the KSPPIPEBCGS implementation only allows for right preconditioning.
    MPI configuration may be necessary for reductions to make asynchronous progress, which is important for
    performance of pipelined methods. See the FAQ on the PETSc website for details.

    Contributed by:
    Siegfried Cools, Universiteit Antwerpen,
    EXA2CT European Project on EXascale Algorithms and Advanced Computational Techniques, 2016.

    Reference:
    S. Cools and W. Vanroose,
    "The communication-hiding pipelined BiCGStab method for the parallel solution of large unsymmetric linear systems",
    Submitted to Parallel Computing, 2016.

.seealso:  KSPCreate(), KSPSetType(), KSPType (for list of available types), KSP, KSPBICG, KSPFBCGS, KSPFBCGSL, KSPSetPCSide()
M*/
PETSC_EXTERN PetscErrorCode KSPCreate_SAPIPEBCGS(KSP ksp)
{
  PetscErrorCode ierr;
  KSP_SABCGS     *bcgs;

  PetscFunctionBegin;
  ierr = PetscNewLog(ksp,&bcgs);CHKERRQ(ierr);

  ksp->data                = bcgs;
  ksp->ops->setup          = KSPSetUp_SAPIPEBCGS;
  ksp->ops->solve          = KSPSolve_SAPIPEBCGS;
  ksp->ops->destroy        = KSPDestroy_SABCGS;
  ksp->ops->reset          = KSPReset_SABCGS;
  ksp->ops->buildresidual  = KSPBuildResidualDefault;
  ksp->ops->setfromoptions = KSPSetFromOptions_SABCGS;

  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_UNPRECONDITIONED,PC_RIGHT,2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
