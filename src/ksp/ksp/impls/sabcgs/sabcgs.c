
#include <../src/ksp/ksp/impls/sabcgs/sabcgsimpl.h>       /*I  "petscksp.h"  I*/
#include <sa_app.h>

PetscErrorCode KSPSetFromOptions_SABCGS(PetscOptionItems *PetscOptionsObject,KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"KSP BCGS Options");CHKERRQ(ierr);
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode KSPSetUp_SABCGS(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPSetWorkVecs(ksp,6);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode KSPSolve_SABCGS(KSP ksp)
{
  PetscErrorCode ierr;
  PetscInt       i;
  PetscScalar    rho,rhoold,alpha,beta,omega,omegaold,d1,mpivals[2];
  Vec            X,B,V,P,R,RP,T,S;
  PetscReal      dp    = 0.0,d2;
  KSP_SABCGS     *bcgs = (KSP_SABCGS*)ksp->data;

  PetscFunctionBegin;
  //int rank;
  //MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  X  = ksp->vec_sol;
  B  = ksp->vec_rhs;
  R  = ksp->work[0];
  RP = ksp->work[1];
  V  = ksp->work[2];
  T  = ksp->work[3];
  S  = ksp->work[4];
  P  = ksp->work[5];

  NT_ITER_INIT();
  NT_ITER_START(NTD_TOTAL);

  /* Compute initial preconditioned residual */
  NT_ITER_INDEX(ierr = KSPInitialResidual(ksp,X,V,T,R,B));CHKERRQ(ierr);

  /* with right preconditioning need to save initial guess to add to final solution */
  if (ksp->pc_side == PC_RIGHT && !ksp->guess_zero) {
    NT_ITER_START(NTD_VECOPS);
    if (!bcgs->guess) {
      ierr = VecDuplicate(X,&bcgs->guess);CHKERRQ(ierr);
    }
    ierr = VecCopy(X,bcgs->guess);CHKERRQ(ierr);
    ierr = VecSet(X,0.0);CHKERRQ(ierr);
    NT_ITER_END(NTD_VECOPS);
  }

  NT_ITER_INDEX2(ierr = VecDot(R,R,&rho),NTD_VECALLR,NTD_VECOPS);CHKERRQ(ierr);       /*   rho <- (r,rp)      */
  dp = PetscSqrtReal(PetscAbsScalar(rho));

  /* Make the initial Rp == R */
  NT_ITER_START(NTD_VECOPS);
  ierr = VecCopy(R,RP);CHKERRQ(ierr);

  rhoold   = 1.0;
  alpha    = 1.0;
  omegaold = 1.0;
  ierr     = VecSet(P,0.0);CHKERRQ(ierr);
  ierr     = VecSet(V,0.0);CHKERRQ(ierr);
  NT_ITER_END(NTD_VECOPS);

  NT_ITER_END(NTD_TOTAL);
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECALLR),NTD_ALLR);
  NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPS),NTD_COMP);
  NT_ITER_ADD(NT_ITER_TIME(NTD_ALLR),NTD_COMM);
  NT_ITER_ADD(NT_ITER_TIME(NTD_TOTAL)-NT_ITER_TIME(NTD_COMM)-NT_ITER_TIME(NTD_COMP),NTD_EXTRA);
  NT_ITER_FINISH();

  ierr       = PetscObjectSAWsTakeAccess((PetscObject)ksp);CHKERRQ(ierr);
  ksp->its   = 0;
  ksp->rnorm = dp;
  //if(rank==0) printf("bcgs iteration=%d norm=%e\n",ksp->its,ksp->rnorm);
  ierr       = PetscObjectSAWsGrantAccess((PetscObject)ksp);CHKERRQ(ierr);
  ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
  ierr = KSPMonitor(ksp,0,dp);CHKERRQ(ierr);
  ierr = (*ksp->converged)(ksp,0,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);
  if (ksp->reason) {
    if (bcgs->guess) {
      NT_ITER_FUNC(ierr = VecAXPY(X,1.0,bcgs->guess),NTD_VECOPS);CHKERRQ(ierr);
    }
    PetscFunctionReturn(0);
  }

  i=0;
  do {
    NT_ITER_INIT();
    NT_ITER_START(NTD_TOTAL);

    beta = (rho/rhoold) * (alpha/omegaold);
    NT_ITER_FUNC(ierr = VecAXPBYPCZ(P,1.0,-omegaold*beta,beta,R,V),NTD_VECOPS);CHKERRQ(ierr); /* p <- r - omega * beta* v + beta * p */
    NT_ITER_FUNC(ierr = KSP_PCApplyBAorAB(ksp,P,V,T),NTD_MVPC1);CHKERRQ(ierr);                 /* v <- K p        */
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END),NTD_MVPCCOMM1);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC),NTD_MVPCCOMP1);

    NT_ITER_INDEX2(ierr = VecDot(V,RP,&d1),NTD_MVPCALLR1,NTD_VECOPS);CHKERRQ(ierr);
    if (d1 == 0.0) {
      if (ksp->errorifnotconverged) SETERRQ(PetscObjectComm((PetscObject)ksp),PETSC_ERR_NOT_CONVERGED,"KSPSolve has not converged due to Nan or Inf inner product");
      else {
        ksp->reason = KSP_DIVERGED_NANORINF;
        break;
      }
    }

    alpha = rho / d1;                                                          /*   a <- rho / (v,rp)  */
    NT_ITER_FUNC(ierr = VecWAXPY(S,-alpha,V,R),NTD_VECOPS);CHKERRQ(ierr);      /*   s <- r - a v       */
    NT_ITER_FUNC(ierr = KSP_PCApplyBAorAB(ksp,S,T,R),NTD_MVPC2);CHKERRQ(ierr); /*   t <- K s    */
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END)
                -NT_ITER_TIME(NTD_MVPCCOMM1),NTD_MVPCCOMM2);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MV_DIAG)+NT_ITER_TIME(NTD_MV_OFFD)
                +NT_ITER_TIME(NTD_PC)-NT_ITER_TIME(NTD_MVPCCOMP1),NTD_MVPCCOMP2);

    NT_ITER_INDEX2(ierr = VecDotNorm2(S,T,&d1,&d2),NTD_MVPCALLR2,NTD_VECOPS);CHKERRQ(ierr); // Compute (S,T) and (T,T)

    if (d2 == 0.0) {
      /* t is 0.  if s is 0, then alpha v == r, and hence alpha p
         may be our solution.  Give it a try? */
      NT_ITER_INDEX2(ierr = VecDot(S,S,&d1),NTD_VECALLR,NTD_VECOPS);CHKERRQ(ierr);
      if (d1 != 0.0) {
        ksp->reason = KSP_DIVERGED_BREAKDOWN;
        break;
      }
      NT_ITER_FUNC(ierr = VecAXPY(X,alpha,P),NTD_VECOPS);CHKERRQ(ierr);   /*   x <- x + a p       */
      ierr = PetscObjectSAWsTakeAccess((PetscObject)ksp);CHKERRQ(ierr);
      ksp->its++;
      ksp->rnorm  = 0.0;
      ksp->reason = KSP_CONVERGED_RTOL;
      ierr = PetscObjectSAWsGrantAccess((PetscObject)ksp);CHKERRQ(ierr);
      ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
      ierr = KSPMonitor(ksp,i+1,0.0);CHKERRQ(ierr);
      break;
    }

    // Set variables
    omega    = d1 / d2;                                        /* w <- (t's) / (t't) */
    rhoold   = rho;
    omegaold = omega;

    /************************************************
     * Merged Vector Operations:
     * x <- alpha * p + omega * s + x
     * r <- s - w t
     * rho <- (r,rp)
     * norm <- (r,r)
     ***********************************************/
    NT_ITER_FUNC(ierr = VecMergedOps_BICGSTAB(X,P,S,R,T,RP,alpha,omega,
                                              &rho,&dp),NTD_VECOPS);CHKERRQ(ierr);

    // Call allreduce
    mpivals[0] = rho;
    mpivals[1] = dp;
    NT_ITER_FUNC(ierr = MPI_Allreduce(MPI_IN_PLACE,mpivals,2,MPI_DOUBLE,
                                      MPI_SUM,PETSC_COMM_WORLD),NTD_VECALLR);CHKERRQ(ierr);
    rho = mpivals[0];
    dp  = PetscSqrtReal(PetscAbsScalar(mpivals[1]));

    NT_ITER_END(NTD_TOTAL);
    NT_ITER_ADD(NT_ITER_TIME(NTD_MVPC1)+NT_ITER_TIME(NTD_MVPC2),NTD_MVPC);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECALLR)+NT_ITER_TIME(NTD_MVPCALLR1)
                +NT_ITER_TIME(NTD_MVPCALLR2),NTD_ALLR);
    NT_ITER_ADD(NT_ITER_TIME(NTD_VECOPS)+NT_ITER_TIME(NTD_MV_DIAG)
                +NT_ITER_TIME(NTD_MV_OFFD)+NT_ITER_TIME(NTD_PC),NTD_COMP);
    NT_ITER_ADD(NT_ITER_TIME(NTD_ALLR)+NT_ITER_TIME(NTD_MV_START)+NT_ITER_TIME(NTD_MV_END),NTD_COMM);
    NT_ITER_ADD(NT_ITER_TIME(NTD_TOTAL)-NT_ITER_TIME(NTD_COMM)-NT_ITER_TIME(NTD_COMP),NTD_EXTRA);
    NT_ITER_FINISH();

    ierr = PetscObjectSAWsTakeAccess((PetscObject)ksp);CHKERRQ(ierr);
    ksp->its++;
    ksp->rnorm = dp;
    //if(rank==0) printf("bcgs iteration=%d norm=%e\n",ksp->its,ksp->rnorm);
    ierr = PetscObjectSAWsGrantAccess((PetscObject)ksp);CHKERRQ(ierr);
    ierr = KSPLogResidualHistory(ksp,dp);CHKERRQ(ierr);
    ierr = KSPMonitor(ksp,i+1,dp);CHKERRQ(ierr);
    ierr = (*ksp->converged)(ksp,i+1,dp,&ksp->reason,ksp->cnvP);CHKERRQ(ierr);
    if (ksp->reason) break;
    if (rho == 0.0) {
      ksp->reason = KSP_DIVERGED_BREAKDOWN;
      break;
    }
    i++;
  } while (i<ksp->max_it);

  if (i >= ksp->max_it) ksp->reason = KSP_DIVERGED_ITS;

  ierr = KSPUnwindPreconditioner(ksp,X,T);CHKERRQ(ierr);
  if (bcgs->guess) {
    ierr = VecAXPY(X,1.0,bcgs->guess);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode KSPBuildSolution_SABCGS(KSP ksp,Vec v,Vec *V)
{
  PetscErrorCode ierr;
  KSP_SABCGS     *bcgs = (KSP_SABCGS*)ksp->data;

  PetscFunctionBegin;
  if (ksp->pc_side == PC_RIGHT) {
    if (v) {
      ierr = KSP_PCApply(ksp,ksp->vec_sol,v);CHKERRQ(ierr);
      if (bcgs->guess) {
        ierr = VecAXPY(v,1.0,bcgs->guess);CHKERRQ(ierr);
      }
      *V = v;
    } else SETERRQ(PetscObjectComm((PetscObject)ksp),PETSC_ERR_SUP,"Not working with right preconditioner");
  } else {
    if (v) {
      ierr = VecCopy(ksp->vec_sol,v);CHKERRQ(ierr); *V = v;
    } else *V = ksp->vec_sol;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode KSPReset_SABCGS(KSP ksp)
{
  KSP_SABCGS     *cg = (KSP_SABCGS*)ksp->data;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&cg->guess);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode KSPDestroy_SABCGS(KSP ksp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = KSPReset_SABCGS(ksp);CHKERRQ(ierr);
  ierr = KSPDestroyDefault(ksp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*MC
     KSPBCGS - Implements the BiCGStab (Stabilized version of BiConjugate Gradient) method.

   Options Database Keys:
.   see KSPSolve()

   Level: beginner

   Notes:
    See KSPBCGSL for additional stabilization
          Supports left and right preconditioning but not symmetric

   References:
.    1. -   van der Vorst, SIAM J. Sci. Stat. Comput., 1992.

.seealso:  KSPCreate(), KSPSetType(), KSPType (for list of available types), KSP, KSPBICG, KSPBCGSL, KSPFBICG, KSPSetPCSide()
M*/
PETSC_EXTERN PetscErrorCode KSPCreate_SABCGS(KSP ksp)
{
  PetscErrorCode ierr;
  KSP_SABCGS     *bcgs;

  PetscFunctionBegin;
  ierr = PetscNewLog(ksp,&bcgs);CHKERRQ(ierr);

  ksp->data                = bcgs;
  ksp->ops->setup          = KSPSetUp_SABCGS;
  ksp->ops->solve          = KSPSolve_SABCGS;
  ksp->ops->destroy        = KSPDestroy_SABCGS;
  ksp->ops->reset          = KSPReset_SABCGS;
  ksp->ops->buildsolution  = KSPBuildSolution_SABCGS;
  ksp->ops->buildresidual  = KSPBuildResidualDefault;
  ksp->ops->setfromoptions = KSPSetFromOptions_SABCGS;

  //ierr = KSPSetSupportedNorm(ksp,KSP_NORM_PRECONDITIONED,PC_LEFT,3);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_UNPRECONDITIONED,PC_RIGHT,2);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_NONE,PC_LEFT,1);CHKERRQ(ierr);
  ierr = KSPSetSupportedNorm(ksp,KSP_NORM_NONE,PC_RIGHT,1);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
