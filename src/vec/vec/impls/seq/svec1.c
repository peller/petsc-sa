/********************************************************************
 * Separate vector operations that load vectors from memory for each
 * operation.
 *******************************************************************/

#include <../src/vec/vec/impls/dvecimpl.h>
#include <petscksp.h>
#include <petscblaslapack.h>

/**********************************************************
 * Compute separate local dot products for CG:
 * beta = z'*r
 * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or beta (Natural)
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecSepDot_CG"
PetscErrorCode VecSepDot_CG(Vec Z,Vec R,PetscInt normtype,
                            PetscScalar *b,PetscScalar *d)
{
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecLocalDot(Z,R,b);CHKERRQ(ierr);
  if(normtype==KSP_NORM_PRECONDITIONED) {
    ierr = VecLocalDot(Z,Z,d);CHKERRQ(ierr);
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
    ierr = VecLocalDot(R,R,d);CHKERRQ(ierr);
  } else if(normtype==KSP_NORM_NATURAL) {
    *d = *b;
  }
  PetscFunctionReturn(0);
}
