/********************************************************************
 * Merged vector operations that load vectors from memory once and
 * use the data multiple times by performing vector operations
 * element-wise.
 *******************************************************************/

#include <../src/vec/vec/impls/dvecimpl.h>
#include <petscksp.h>
#include <petscblaslapack.h>

#undef __FUNCT__
#define __FUNCT__ "VecLocalDot"
/**********************************************************
 * Compute a local dot product
 *********************************************************/
PetscErrorCode VecLocalDot(Vec v1,Vec v2,PetscScalar *dot1)
{
  const PetscScalar * __restrict pv1,* __restrict pv2;
  PetscScalar       sum1=0.0;
  PetscInt          i,n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(v1,(const PetscScalar**)&pv1);CHKERRQ(ierr);
  ierr = VecGetArrayRead(v2,(const PetscScalar**)&pv2);CHKERRQ(ierr);
  ierr = VecGetLocalSize(v1,&n);CHKERRQ(ierr);

#pragma _CRI ivdep
  for(i=0; i<n; i++) {
    sum1 += pv1[i]*pv2[i];
  }
  *dot1 = sum1;

  ierr = VecRestoreArrayRead(v1,(const PetscScalar**)&pv1);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(v2,(const PetscScalar**)&pv2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/**********************************************************
 * Compute merged local dot products for CG:
 * beta = z'*r
 * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or beta (Natural)
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedDot_CG"
PetscErrorCode VecMergedDot_CG(Vec Z,Vec R,PetscInt normtype,
                               PetscScalar *b,PetscScalar *d)
{
  const PetscScalar * __restrict PZ,* __restrict PR;
  PetscScalar       sumb=0.0,sumd=0.0;
  PetscInt          i,n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetLocalSize(Z,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumb += PZ[i]*PR[i];
      sumd += PZ[i]*PZ[i];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumb += PZ[i]*PR[i];
      sumd += PR[i]*PR[i];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumb += PZ[i]*PR[i];
    }
    sumd = sumb;
  }

  *b = sumb;
  *d = sumd;

  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*********************************************
 * Compute merged vector operations for CG:
 * x <- x + ap
 * p <- z + b* p
 ********************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_CG"
PetscErrorCode VecMergedOps_CG(Vec X,Vec P,Vec Z,PetscScalar a,PetscScalar b) {

  PetscScalar       * __restrict PX, * __restrict PP;
  const PetscScalar * __restrict PZ;
  int               i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecGetArray(P,(PetscScalar**)&PP);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetLocalSize(X,&n);CHKERRQ(ierr);

#pragma _CRI ivdep
  for(i=0; i<n; i++) {
    PX[i] = PX[i] + a*PP[i];
    PP[i] = PZ[i] + b*PP[i];
  }

  ierr = VecRestoreArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecRestoreArray(P,(PetscScalar**)&PP);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/************************************************************
 * Compute merged local dot products for SAPCG
 * (single allreduce PCG):
 * delta = z'*s (z'*A*z = r'*B*A*B*r)
 * beta  = z'*r
 * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or sqrt(beta) (Natural)
 ***********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedDot_SAPCG"
PetscErrorCode VecMergedDot_SAPCG(Vec Z,Vec S,Vec R,PetscInt normtype,
                                  PetscScalar *d,PetscScalar *b,PetscScalar *p)
{
  const PetscScalar * __restrict PZ,* __restrict PS, * __restrict PR;
  PetscScalar       sumd=0.0,sumb=0.0,sump=0.0;
  PetscInt          i,n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetArrayRead(S,(const PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecGetArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetLocalSize(Z,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumd += PZ[i]*PS[i];
      sumb += PZ[i]*PR[i];
      sump += PZ[i]*PZ[i];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumd += PZ[i]*PS[i];
      sumb += PZ[i]*PR[i];
      sump += PR[i]*PR[i];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumd += PZ[i]*PS[i];
      sumb += PZ[i]*PR[i];
    }
    sump = sumb;
  }

  *d = sumd;
  *b = sumb;
  *p = sump;

  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(S,(const PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/***********************************************
 * Compute merged vector operations for SAPCG
 * (single allreduce PCG):
 * p <- z + b*p
 * w <- s + b*w
 * x <- x + a*p
 * r <- r - a*w
 **********************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_SAPCG"
PetscErrorCode VecMergedOps_SAPCG(Vec P,Vec Z,Vec W,Vec S,Vec X,Vec R,
                                  PetscScalar b,PetscScalar a) {

  PetscScalar       * __restrict PP, * __restrict PW;
  PetscScalar       * __restrict PX, * __restrict PR;
  const PetscScalar * __restrict PZ, * __restrict PS;
  int               i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(P,(PetscScalar**)&PP);CHKERRQ(ierr);
  ierr = VecGetArray(W,(PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecGetArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecGetArray(R,(PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetArrayRead(S,(const PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecGetLocalSize(P,&n);CHKERRQ(ierr);

#pragma _CRI ivdep
  for(i=0; i<n; i++) {
    PP[i] = PZ[i] + b*PP[i];
    PW[i] = PS[i] + b*PW[i];
    PX[i] = PX[i] + a*PP[i];
    PR[i] = PR[i] - a*PW[i];
  }

  ierr = VecRestoreArray(P,(PetscScalar**)&PP);CHKERRQ(ierr);
  ierr = VecRestoreArray(W,(PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecRestoreArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecRestoreArray(R,(PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(S,(const PetscScalar**)&PS);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/**********************************************************
 * Compute merged local dot products for SANBPCG:
 * delta = z'*s (z'*A*z = r'*B*A*B*r)
 * beta  = z'*r
 * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or sqrt(beta) (Natural)
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedDot_SANBPCG"
PetscErrorCode VecMergedDot_SANBPCG(Vec R,Vec Z,PetscInt normtype,
                                    PetscScalar *g,PetscScalar *d)
{
  const PetscScalar * __restrict PR,* __restrict PZ;
  PetscScalar       sumg=0.0,sumd=0.0;
  PetscInt          i,n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetLocalSize(R,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumg += PR[i]*PZ[i];
      sumd += PZ[i]*PZ[i];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumg += PR[i]*PZ[i];
      sumd += PR[i]*PR[i];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumg += PR[i]*PZ[i];
    }
    sumd = sumg;
  }

  *g = sumg;
  *d = sumd;

  ierr = VecRestoreArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/***************************************************
 * Compute merged vector operations for SANBPCG:
 * x = x + a*p
 * p = z + b*p
 * s = Z + b*s
 * delta = p'*s
 **************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps1_SANBPCG"
PetscErrorCode VecMergedOps1_SANBPCG(Vec x,Vec p,Vec z,Vec s,Vec Z,
                                     PetscScalar a,PetscScalar b,PetscScalar *d) {

  PetscScalar       * __restrict Px, * __restrict Pp, * __restrict Ps;
  const PetscScalar * __restrict Pz, * __restrict PZ;
  PetscScalar       sumd = 0.0;
  PetscInt          i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(x,(PetscScalar**)&Px);CHKERRQ(ierr);
  ierr = VecGetArray(p,(PetscScalar**)&Pp);CHKERRQ(ierr);
  ierr = VecGetArray(s,(PetscScalar**)&Ps);CHKERRQ(ierr);
  ierr = VecGetArrayRead(z,(const PetscScalar**)&Pz);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetLocalSize(x,&n);CHKERRQ(ierr);

#pragma _CRI ivdep
  for(i=0; i<n; i++) {
    Px[i] = Px[i] + a*Pp[i];
    Pp[i] = Pz[i] + b*Pp[i];
    Ps[i] = PZ[i] + b*Ps[i];
    sumd += Pp[i]*Ps[i];
  }

  *d = sumd;

  ierr = VecRestoreArray(x,(PetscScalar**)&Px);CHKERRQ(ierr);
  ierr = VecRestoreArray(p,(PetscScalar**)&Pp);CHKERRQ(ierr);
  ierr = VecRestoreArray(s,(PetscScalar**)&Ps);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(z,(const PetscScalar**)&Pz);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/************************************************************
 * Compute merged vector operations for SANBPCG:
 * r = r - a*s
 * z = z - a*S
 * gamma = r'*z
 * dp = ||z|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or sqrt(gamma) (Natural)
 ***********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps2_SANBPCG"
PetscErrorCode VecMergedOps2_SANBPCG(Vec r,Vec s,Vec z,Vec S,PetscInt normtype,
                                     PetscScalar a,PetscScalar *g,PetscScalar *d) {

  PetscScalar       * __restrict Pr, * __restrict Pz;
  const PetscScalar * __restrict Ps, * __restrict PS;
  PetscScalar       sumg = 0.0, sumd = 0.0;
  PetscInt          i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(r,(PetscScalar**)&Pr);CHKERRQ(ierr);
  ierr = VecGetArray(z,(PetscScalar**)&Pz);CHKERRQ(ierr);
  ierr = VecGetArrayRead(s,(const PetscScalar**)&Ps);CHKERRQ(ierr);
  ierr = VecGetArrayRead(S,(const PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecGetLocalSize(r,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      Pr[i] = Pr[i] - a*Ps[i];
      Pz[i] = Pz[i] - a*PS[i];
      sumg += Pr[i]*Pz[i];
      sumd += Pz[i]*Pz[i];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      Pr[i] = Pr[i] - a*Ps[i];
      Pz[i] = Pz[i] - a*PS[i];
      sumg += Pr[i]*Pz[i];
      sumd += Pr[i]*Pr[i];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      Pr[i] = Pr[i] - a*Ps[i];
      Pz[i] = Pz[i] - a*PS[i];
      sumg += Pr[i]*Pz[i];
    }
    sumd = sumg;
  }

  *g = sumg;
  *d = sumd;

  ierr = VecRestoreArray(r,(PetscScalar**)&Pr);CHKERRQ(ierr);
  ierr = VecRestoreArray(z,(PetscScalar**)&Pz);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(s,(const PetscScalar**)&Ps);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(S,(const PetscScalar**)&PS);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/***********************************************************
 * Compute merged local dot products for SAPIPECG:
 * gamma = r'*u
 * delta = w'*u
 * dp = ||u|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or sqrt(gamma) (Natural)
 **********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedDot_SAPIPECG"
PetscErrorCode VecMergedDot_SAPIPECG(Vec R,Vec U,Vec W,PetscInt normtype,
                                     PetscScalar *g,PetscScalar *d,PetscScalar *p)
{
  const PetscScalar * __restrict PR,* __restrict PU,* __restrict PW;
  PetscScalar       sumg=0.0,sumd=0.0,sump=0.0;
  PetscInt          i,n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetArrayRead(U,(const PetscScalar**)&PU);CHKERRQ(ierr);
  ierr = VecGetArrayRead(W,(const PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecGetLocalSize(R,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumg += PR[i]*PU[i];
      sumd += PW[i]*PU[i];
      sump += PU[i]*PU[i];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumg += PR[i]*PU[i];
      sumd += PW[i]*PU[i];
      sump += PR[i]*PR[i];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      sumg += PR[i]*PU[i];
      sumd += PW[i]*PU[i];
    }
    sump = sumg;
  }

  *g = sumg;
  *d = sumd;
  *p = sump;

  ierr = VecRestoreArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(U,(const PetscScalar**)&PU);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(W,(const PetscScalar**)&PW);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/************************************************************
 * Compute merged vector operations for SAPIPECG:
 * z = n + b*z     q = m + b*q
 * p = u + b*p     s = w + b*s
 * x = x + a*p     u = u - a*q
 * w = w - a*z     r = r - a*s
 * gamma = r'*u    delta = w'*u
 * dp = ||u|| (Preconditioned) or ||r|| (Unpreconditioned)
 *      or sqrt(gamma) (Natural)
 ***********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_SAPIPECG"
PetscErrorCode VecMergedOps_SAPIPECG(Vec Z,Vec Q,Vec P,Vec S,Vec X,Vec U, Vec W,
                                     Vec R,Vec N,Vec M, PetscInt normtype,
                                     PetscScalar b,PetscScalar a,PetscScalar *g,
                                     PetscScalar *d,PetscScalar *p) {

  PetscScalar       * __restrict PZ, * __restrict PQ, * __restrict PP, * __restrict PS;
  PetscScalar       * __restrict PX, * __restrict PU, * __restrict PW, * __restrict PR;
  const PetscScalar * __restrict PN, * __restrict PM;
  PetscScalar       sumg = 0.0, sumd = 0.0, sump = 0.0;
  PetscInt          i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(Z,(PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetArray(Q,(PetscScalar**)&PQ);CHKERRQ(ierr);
  ierr = VecGetArray(P,(PetscScalar**)&PP);CHKERRQ(ierr);
  ierr = VecGetArray(S,(PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecGetArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecGetArray(U,(PetscScalar**)&PU);CHKERRQ(ierr);
  ierr = VecGetArray(W,(PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecGetArray(R,(PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetArrayRead(N,(const PetscScalar**)&PN);CHKERRQ(ierr);
  ierr = VecGetArrayRead(M,(const PetscScalar**)&PM);CHKERRQ(ierr);
  ierr = VecGetLocalSize(Z,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      PZ[i] = PN[i] + b*PZ[i];
      PQ[i] = PM[i] + b*PQ[i];
      PP[i] = PU[i] + b*PP[i];
      PS[i] = PW[i] + b*PS[i];
      PX[i] = PX[i] + a*PP[i];
      PU[i] = PU[i] - a*PQ[i];
      PW[i] = PW[i] - a*PZ[i];
      PR[i] = PR[i] - a*PS[i];
      sumg += PR[i]*PU[i];
      sumd += PW[i]*PU[i];
      sump += PU[i]*PU[i];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      PZ[i] = PN[i] + b*PZ[i];
      PQ[i] = PM[i] + b*PQ[i];
      PP[i] = PU[i] + b*PP[i];
      PS[i] = PW[i] + b*PS[i];
      PX[i] = PX[i] + a*PP[i];
      PU[i] = PU[i] - a*PQ[i];
      PW[i] = PW[i] - a*PZ[i];
      PR[i] = PR[i] - a*PS[i];
      sumg += PR[i]*PU[i];
      sumd += PW[i]*PU[i];
      sump += PR[i]*PR[i];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(i=0; i<n; i++) {
      PZ[i] = PN[i] + b*PZ[i];
      PQ[i] = PM[i] + b*PQ[i];
      PP[i] = PU[i] + b*PP[i];
      PS[i] = PW[i] + b*PS[i];
      PX[i] = PX[i] + a*PP[i];
      PU[i] = PU[i] - a*PQ[i];
      PW[i] = PW[i] - a*PZ[i];
      PR[i] = PR[i] - a*PS[i];
      sumg += PR[i]*PU[i];
      sumd += PW[i]*PU[i];
    }
    sump = sumg;
  }

  *g = sumg;
  *d = sumd;
  *p = sump;

  ierr = VecRestoreArray(Z,(PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecRestoreArray(Q,(PetscScalar**)&PQ);CHKERRQ(ierr);
  ierr = VecRestoreArray(P,(PetscScalar**)&PP);CHKERRQ(ierr);
  ierr = VecRestoreArray(S,(PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecRestoreArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecRestoreArray(U,(PetscScalar**)&PU);CHKERRQ(ierr);
  ierr = VecRestoreArray(W,(PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecRestoreArray(R,(PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(N,(const PetscScalar**)&PN);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(M,(const PetscScalar**)&PM);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/****************************************************
 * Compute merged local dot products for SAPIPE2CG:
 * delta <- z'*w
 * beta  <- z'*r
 * dp    <- ||z||
 ***************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedDot_SAPIPE2CG"
PetscErrorCode VecMergedDot_SAPIPE2CG(Vec Z,Vec W,Vec R,PetscInt normtype,PetscScalar *d,
                                      PetscScalar *b,PetscScalar *p)
{
  const PetscScalar *__restrict PZ, *__restrict PW, *__restrict PR;
  PetscScalar       sumd = 0.0, sumb = 0.0, sump = 0.0;
  PetscInt          j, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecGetArrayRead(W,(const PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecGetArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetLocalSize(Z,&n);CHKERRQ(ierr);

  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      sumd += PZ[j] * PW[j];
      sumb += PZ[j] * PR[j];
      sump += PZ[j] * PZ[j];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      sumd += PZ[j] * PW[j];
      sumb += PZ[j] * PR[j];
      sump += PR[j] * PR[j];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      sumd += PZ[j] * PW[j];
      sumb += PZ[j] * PR[j];
    }
    sump = sumb;
  }

  *d = sumd;
  *b = sumb;
  *p = sump;

  ierr = VecRestoreArrayRead(Z,(const PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(W,(const PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#define VECS_GET_ARRAY_READ1(v,pv,pv1,pv2)                              \
  ierr = VecGetArray(v[0],(PetscScalar**)&pv);CHKERRQ(ierr);            \
  ierr = VecGetArray(v[1],(PetscScalar**)&pv1);CHKERRQ(ierr);           \
  ierr = VecGetArrayRead(v[2],(const PetscScalar**)&pv2);CHKERRQ(ierr);

#define VECS_RESTORE_ARRAY_READ1(v,pv,pv1,pv2)                          \
  ierr = VecRestoreArray(v[0],(PetscScalar**)&pv);CHKERRQ(ierr);        \
  ierr = VecRestoreArray(v[1],(PetscScalar**)&pv1);CHKERRQ(ierr);       \
  ierr = VecRestoreArrayRead(v[2],(const PetscScalar**)&pv2);CHKERRQ(ierr);

#define VECS_GET_ARRAY_READ21(v,pv,pv1,pv2) \
  ierr = VecGetArrayRead(v[0],(const PetscScalar**)&pv);CHKERRQ(ierr); \
  ierr = VecGetArray(v[1],(PetscScalar**)&pv1);CHKERRQ(ierr); \
  ierr = VecGetArrayRead(v[2],(const PetscScalar**)&pv2);CHKERRQ(ierr);

#define VECS_RESTORE_ARRAY_READ21(v,pv,pv1,pv2) \
  ierr = VecRestoreArrayRead(v[0],(const PetscScalar**)&pv);CHKERRQ(ierr); \
  ierr = VecRestoreArray(v[1],(PetscScalar**)&pv1);CHKERRQ(ierr); \
  ierr = VecRestoreArrayRead(v[2],(const PetscScalar**)&pv2);CHKERRQ(ierr);

#define VECS_GET_ARRAY_READ20(v,pv,pv1,pv2) \
  ierr = VecGetArray(v[0],(PetscScalar**)&pv);CHKERRQ(ierr); \
  ierr = VecGetArrayRead(v[1],(const PetscScalar**)&pv1);CHKERRQ(ierr); \
  ierr = VecGetArrayRead(v[2],(const PetscScalar**)&pv2);CHKERRQ(ierr);

#define VECS_RESTORE_ARRAY_READ20(v,pv,pv1,pv2) \
  ierr = VecRestoreArray(v[0],(PetscScalar**)&pv);CHKERRQ(ierr); \
  ierr = VecRestoreArrayRead(v[1],(const PetscScalar**)&pv1);CHKERRQ(ierr); \
  ierr = VecRestoreArrayRead(v[2],(const PetscScalar**)&pv2);CHKERRQ(ierr);

// Swap vectors once
#define SWAP_VECS1(v,temp) \
  temp = v[2];            \
  v[2] = v[1];            \
  v[1] = v[0];            \
  v[0] = temp;

// Swap vectors twice
#define SWAP_VECS2(v,temp) \
  temp = v[0];             \
  v[0] = v[1];             \
  v[1] = v[2];             \
  v[2] = temp;

/**********************************************************
 * Compute merged vector operations for the pipeline
 * for PIPE2CG:
 *
 * x1 <- rho2 * (x2 + gamma2 * z2) + (1.0-rho2) * x
 * r1 <- rho2 * (r2 - gamma2 * w2) + (1.0-rho2) * r
 * z1 <- rho2 * (z2 - gamma2 * p2) + (1.0-rho2) * z
 * w1 <- rho2 * (w2 - gamma2 * q2) + (1.0-rho2) * w
 * p1 <- rho2 * (p2 - gamma2 * c1) + (1.0-rho2) * p
 * q1 <- rho2 * (q2 - gamma2 * d1) + (1.0-rho2) * q
 * c  <- rho2 * (c1 - gamma2 * g)  + (1.0-rho2) * c2
 * d  <- rho2 * (d1 - gamma2 * h)  + (1.0-rho2) * d2
 * x  <- rho * (x1 + gamma * z1) + (1.0-rho) * x2
 * r  <- rho * (r1 - gamma * w1) + (1.0-rho) * r2
 * z  <- rho * (z1 - gamma * p1) + (1.0-rho) * z2
 * w  <- rho * (w1 - gamma * q1) + (1.0-rho) * w2
 * p  <- rho * (p1 - gamma * c)  + (1.0-rho) * p2
 * q  <- rho * (q1 - gamma * d)  + (1.0-rho) * q2
 * dot1  <- z  * r   dot2  <- z  * z
 * dot3  <- z  * q   dot4  <- z  * w1
 * dot5  <- p  * q   dot6  <- z1 * q
 * dot7  <- z1 * w1  dot8  <- z  * r1
 * dot9  <- p  * r   dot10 <- z1 * r1
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOpsShort_SAPIPE2CG"
PetscErrorCode VecMergedOpsShort_SAPIPE2CG(Vec *vx,Vec *vr,Vec *vz,Vec *vw,Vec *vp,Vec *vq,Vec *vc,
                                           Vec *vd,PetscInt normtype,PetscScalar *rho,
                                           PetscScalar *gamma,PetscScalar *lambda)
{
  PetscScalar       *__restrict px, *__restrict pr, *__restrict pz, *__restrict pw;
  PetscScalar       *__restrict pp, *__restrict pq;
  const PetscScalar *__restrict px1,*__restrict pr1,*__restrict pz1,*__restrict pw1;
  const PetscScalar *__restrict pp1,*__restrict pq1;
  const PetscScalar *__restrict pc1,*__restrict pd1;
  const PetscScalar *__restrict px2,*__restrict pr2,*__restrict pz2,*__restrict pw2;
  const PetscScalar *__restrict pp2,*__restrict pq2;
  PetscInt          j, n;
  PetscErrorCode    ierr;
  Vec               temp;

  PetscFunctionBegin;
  // Swap vectors
  SWAP_VECS1(vx,temp); SWAP_VECS1(vr,temp); SWAP_VECS1(vz,temp); SWAP_VECS1(vw,temp);
  SWAP_VECS1(vp,temp); SWAP_VECS1(vq,temp); SWAP_VECS1(vc,temp); SWAP_VECS1(vd,temp);

  // Get arrays from vectors
  VECS_GET_ARRAY_READ20(vx,px,px1,px2); VECS_GET_ARRAY_READ20(vr,pr,pr1,pr2);
  VECS_GET_ARRAY_READ20(vz,pz,pz1,pz2); VECS_GET_ARRAY_READ20(vw,pw,pw1,pw2);
  VECS_GET_ARRAY_READ20(vp,pp,pp1,pp2); VECS_GET_ARRAY_READ20(vq,pq,pq1,pq2);
  ierr = VecGetArrayRead(vc[1],(const PetscScalar**)&pc1);CHKERRQ(ierr);
  ierr = VecGetArrayRead(vd[1],(const PetscScalar**)&pd1);CHKERRQ(ierr);

  ierr = VecGetLocalSize(vx[0],&n);CHKERRQ(ierr);
  for(j=0; j<10; j++) lambda[j] = 0.0;

  // Compute vector operations
  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      px[j]  = rho[0] * (px1[j] + gamma[0] * pz1[j]) + (1.0-rho[0]) * px2[j];
      pr[j]  = rho[0] * (pr1[j] - gamma[0] * pw1[j]) + (1.0-rho[0]) * pr2[j];
      pz[j]  = rho[0] * (pz1[j] - gamma[0] * pp1[j]) + (1.0-rho[0]) * pz2[j];
      pw[j]  = rho[0] * (pw1[j] - gamma[0] * pq1[j]) + (1.0-rho[0]) * pw2[j];
      pp[j]  = rho[0] * (pp1[j] - gamma[0] * pc1[j]) + (1.0-rho[0]) * pp2[j];
      pq[j]  = rho[0] * (pq1[j] - gamma[0] * pd1[j]) + (1.0-rho[0]) * pq2[j];

      lambda[0] += pz[j]  * pw[j];  lambda[1] += pz[j]  * pq[j];
      lambda[2] += pz[j]  * pw1[j]; lambda[3] += pp[j]  * pq[j];
      lambda[4] += pp[j]  * pw1[j]; lambda[5] += pz1[j] * pw1[j];
      lambda[6] += pz[j]  * pr[j];  lambda[7] += pz[j]  * pr1[j];
      lambda[8] += pz1[j] * pr1[j]; lambda[9] += pz[j]  * pz[j];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      px[j]  = rho[0] * (px1[j] + gamma[0] * pz1[j]) + (1.0-rho[0]) * px2[j];
      pr[j]  = rho[0] * (pr1[j] - gamma[0] * pw1[j]) + (1.0-rho[0]) * pr2[j];
      pz[j]  = rho[0] * (pz1[j] - gamma[0] * pp1[j]) + (1.0-rho[0]) * pz2[j];
      pw[j]  = rho[0] * (pw1[j] - gamma[0] * pq1[j]) + (1.0-rho[0]) * pw2[j];
      pp[j]  = rho[0] * (pp1[j] - gamma[0] * pc1[j]) + (1.0-rho[0]) * pp2[j];
      pq[j]  = rho[0] * (pq1[j] - gamma[0] * pd1[j]) + (1.0-rho[0]) * pq2[j];

      lambda[0] += pz[j]  * pw[j];  lambda[1] += pz[j]  * pq[j];
      lambda[2] += pz[j]  * pw1[j]; lambda[3] += pp[j]  * pq[j];
      lambda[4] += pp[j]  * pw1[j]; lambda[5] += pz1[j] * pw1[j];
      lambda[6] += pz[j]  * pr[j];  lambda[7] += pz[j]  * pr1[j];
      lambda[8] += pz1[j] * pr1[j]; lambda[9] += pr[j]  * pr[j];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      px[j]  = rho[0] * (px1[j] + gamma[0] * pz1[j]) + (1.0-rho[0]) * px2[j];
      pr[j]  = rho[0] * (pr1[j] - gamma[0] * pw1[j]) + (1.0-rho[0]) * pr2[j];
      pz[j]  = rho[0] * (pz1[j] - gamma[0] * pp1[j]) + (1.0-rho[0]) * pz2[j];
      pw[j]  = rho[0] * (pw1[j] - gamma[0] * pq1[j]) + (1.0-rho[0]) * pw2[j];
      pp[j]  = rho[0] * (pp1[j] - gamma[0] * pc1[j]) + (1.0-rho[0]) * pp2[j];
      pq[j]  = rho[0] * (pq1[j] - gamma[0] * pd1[j]) + (1.0-rho[0]) * pq2[j];

      lambda[0] += pz[j]  * pw[j];  lambda[1] += pz[j]  * pq[j];
      lambda[2] += pz[j]  * pw1[j]; lambda[3] += pp[j]  * pq[j];
      lambda[4] += pp[j]  * pw1[j]; lambda[5] += pz1[j] * pw1[j];
      lambda[6] += pz[j]  * pr[j];  lambda[7] += pz[j]  * pr1[j];
      lambda[8] += pz1[j] * pr1[j];
    }
    lambda[9] = lambda[6];
  }

  // Restore vectors
  VECS_RESTORE_ARRAY_READ20(vx,px,px1,px2); VECS_RESTORE_ARRAY_READ20(vr,pr,pr1,pr2);
  VECS_RESTORE_ARRAY_READ20(vz,pz,pz1,pz2); VECS_RESTORE_ARRAY_READ20(vw,pw,pw1,pw2);
  VECS_RESTORE_ARRAY_READ20(vp,pp,pp1,pp2); VECS_RESTORE_ARRAY_READ20(vq,pq,pq1,pq2);
  ierr = VecRestoreArrayRead(vc[1],(const PetscScalar**)&pc1);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(vd[1],(const PetscScalar**)&pd1);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/**********************************************************
 * Compute merged vector operations for the pipeline
 * for PIPE2CG:
 *
 * x1 <- rho2 * (x2 + gamma2 * z2) + (1.0-rho2) * x
 * r1 <- rho2 * (r2 - gamma2 * w2) + (1.0-rho2) * r
 * z1 <- rho2 * (z2 - gamma2 * p2) + (1.0-rho2) * z
 * w1 <- rho2 * (w2 - gamma2 * q2) + (1.0-rho2) * w
 * p1 <- rho2 * (p2 - gamma2 * c1) + (1.0-rho2) * p
 * q1 <- rho2 * (q2 - gamma2 * d1) + (1.0-rho2) * q
 * c  <- rho2 * (c1 - gamma2 * g)  + (1.0-rho2) * c2
 * d  <- rho2 * (d1 - gamma2 * h)  + (1.0-rho2) * d2
 * x  <- rho * (x1 + gamma * z1) + (1.0-rho) * x2
 * r  <- rho * (r1 - gamma * w1) + (1.0-rho) * r2
 * z  <- rho * (z1 - gamma * p1) + (1.0-rho) * z2
 * w  <- rho * (w1 - gamma * q1) + (1.0-rho) * w2
 * p  <- rho * (p1 - gamma * c)  + (1.0-rho) * p2
 * q  <- rho * (q1 - gamma * d)  + (1.0-rho) * q2
 * dot1  <- z  * r   dot2  <- z  * z
 * dot3  <- z  * q   dot4  <- z  * w1
 * dot5  <- p  * q   dot6  <- z1 * q
 * dot7  <- z1 * w1  dot8  <- z  * r1
 * dot9  <- p  * r   dot10 <- z1 * r1
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_SAPIPE2CG"
PetscErrorCode VecMergedOps_SAPIPE2CG(Vec *vx,Vec *vr,Vec *vz,Vec *vw,Vec *vp,Vec *vq,Vec *vc,
                                      Vec *vd,Vec vg,Vec vh,PetscInt normtype,PetscScalar *rho,
                                      PetscScalar *gamma,PetscScalar *lambda)
{
  PetscScalar       *__restrict px, *__restrict pr, *__restrict pz, *__restrict pw;
  PetscScalar       *__restrict pp, *__restrict pq;
  const PetscScalar *__restrict pc, *__restrict pd, *__restrict pg, *__restrict ph;
  PetscScalar       *__restrict px1,*__restrict pr1,*__restrict pz1,*__restrict pw1;
  PetscScalar       *__restrict pp1,*__restrict pq1,*__restrict pc1,*__restrict pd1;
  const PetscScalar *__restrict px2,*__restrict pr2,*__restrict pz2,*__restrict pw2;
  const PetscScalar *__restrict pp2,*__restrict pq2,*__restrict pc2,*__restrict pd2;
  PetscInt          j, n;
  PetscErrorCode    ierr;
  Vec               temp;

  PetscFunctionBegin;
  // Swap vectors
  SWAP_VECS2(vx,temp); SWAP_VECS2(vr,temp); SWAP_VECS2(vz,temp); SWAP_VECS2(vw,temp);
  SWAP_VECS2(vp,temp); SWAP_VECS2(vq,temp); SWAP_VECS2(vc,temp); SWAP_VECS2(vd,temp);

  // Get arrays from vectors
  VECS_GET_ARRAY_READ1(vx,px,px1,px2);  VECS_GET_ARRAY_READ1(vr,pr,pr1,pr2);
  VECS_GET_ARRAY_READ1(vz,pz,pz1,pz2);  VECS_GET_ARRAY_READ1(vw,pw,pw1,pw2);
  VECS_GET_ARRAY_READ1(vp,pp,pp1,pp2);  VECS_GET_ARRAY_READ1(vq,pq,pq1,pq2);
  VECS_GET_ARRAY_READ21(vc,pc,pc1,pc2); VECS_GET_ARRAY_READ21(vd,pd,pd1,pd2);
  ierr = VecGetArrayRead(vg,(const PetscScalar**)&pg);CHKERRQ(ierr);
  ierr = VecGetArrayRead(vh,(const PetscScalar**)&ph);CHKERRQ(ierr);

  ierr = VecGetLocalSize(vx[0],&n);CHKERRQ(ierr);
  for(j=0; j<10; j++) lambda[j] = 0.0;

  // Compute vector operations
  if(normtype==KSP_NORM_PRECONDITIONED) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      px1[j] = rho[1] * (px2[j] + gamma[1] * pz2[j]) + (1.0-rho[1]) * px[j];
      pr1[j] = rho[1] * (pr2[j] - gamma[1] * pw2[j]) + (1.0-rho[1]) * pr[j];
      pz1[j] = rho[1] * (pz2[j] - gamma[1] * pp2[j]) + (1.0-rho[1]) * pz[j];
      pw1[j] = rho[1] * (pw2[j] - gamma[1] * pq2[j]) + (1.0-rho[1]) * pw[j];
      pp1[j] = rho[1] * (pp2[j] - gamma[1] * pc2[j]) + (1.0-rho[1]) * pp[j];
      pq1[j] = rho[1] * (pq2[j] - gamma[1] * pd2[j]) + (1.0-rho[1]) * pq[j];
      pc1[j] = rho[1] * (pc2[j] - gamma[1] * pg[j])  + (1.0-rho[1]) * pc[j];
      pd1[j] = rho[1] * (pd2[j] - gamma[1] * ph[j])  + (1.0-rho[1]) * pd[j];

      px[j]  = rho[0] * (px1[j] + gamma[0]  * pz1[j]) + (1.0-rho[0]) * px2[j];
      pr[j]  = rho[0] * (pr1[j] - gamma[0]  * pw1[j]) + (1.0-rho[0]) * pr2[j];
      pz[j]  = rho[0] * (pz1[j] - gamma[0]  * pp1[j]) + (1.0-rho[0]) * pz2[j];
      pw[j]  = rho[0] * (pw1[j] - gamma[0]  * pq1[j]) + (1.0-rho[0]) * pw2[j];
      pp[j]  = rho[0] * (pp1[j] - gamma[0]  * pc1[j]) + (1.0-rho[0]) * pp2[j];
      pq[j]  = rho[0] * (pq1[j] - gamma[0]  * pd1[j]) + (1.0-rho[0]) * pq2[j];

      lambda[0] += pz[j]  * pw[j];  lambda[1] += pz[j]  * pq[j];
      lambda[2] += pz[j]  * pw1[j]; lambda[3] += pp[j]  * pq[j];
      lambda[4] += pp[j]  * pw1[j]; lambda[5] += pz1[j] * pw1[j];
      lambda[6] += pz[j]  * pr[j];  lambda[7] += pz[j]  * pr1[j];
      lambda[8] += pz1[j] * pr1[j]; lambda[9] += pz[j]  * pz[j];
    }
  } else if(normtype==KSP_NORM_UNPRECONDITIONED) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      px1[j] = rho[1] * (px2[j] + gamma[1] * pz2[j]) + (1.0-rho[1]) * px[j];
      pr1[j] = rho[1] * (pr2[j] - gamma[1] * pw2[j]) + (1.0-rho[1]) * pr[j];
      pz1[j] = rho[1] * (pz2[j] - gamma[1] * pp2[j]) + (1.0-rho[1]) * pz[j];
      pw1[j] = rho[1] * (pw2[j] - gamma[1] * pq2[j]) + (1.0-rho[1]) * pw[j];
      pp1[j] = rho[1] * (pp2[j] - gamma[1] * pc2[j]) + (1.0-rho[1]) * pp[j];
      pq1[j] = rho[1] * (pq2[j] - gamma[1] * pd2[j]) + (1.0-rho[1]) * pq[j];
      pc1[j] = rho[1] * (pc2[j] - gamma[1] * pg[j])  + (1.0-rho[1]) * pc[j];
      pd1[j] = rho[1] * (pd2[j] - gamma[1] * ph[j])  + (1.0-rho[1]) * pd[j];

      px[j]  = rho[0] * (px1[j] + gamma[0]  * pz1[j]) + (1.0-rho[0]) * px2[j];
      pr[j]  = rho[0] * (pr1[j] - gamma[0]  * pw1[j]) + (1.0-rho[0]) * pr2[j];
      pz[j]  = rho[0] * (pz1[j] - gamma[0]  * pp1[j]) + (1.0-rho[0]) * pz2[j];
      pw[j]  = rho[0] * (pw1[j] - gamma[0]  * pq1[j]) + (1.0-rho[0]) * pw2[j];
      pp[j]  = rho[0] * (pp1[j] - gamma[0]  * pc1[j]) + (1.0-rho[0]) * pp2[j];
      pq[j]  = rho[0] * (pq1[j] - gamma[0]  * pd1[j]) + (1.0-rho[0]) * pq2[j];

      lambda[0] += pz[j]  * pw[j];  lambda[1] += pz[j]  * pq[j];
      lambda[2] += pz[j]  * pw1[j]; lambda[3] += pp[j]  * pq[j];
      lambda[4] += pp[j]  * pw1[j]; lambda[5] += pz1[j] * pw1[j];
      lambda[6] += pz[j]  * pr[j];  lambda[7] += pz[j]  * pr1[j];
      lambda[8] += pz1[j] * pr1[j]; lambda[9] += pr[j]  * pr[j];
    }
  } else if(normtype==KSP_NORM_NATURAL) {
#pragma _CRI ivdep
    for(j=0; j<n; j++) {
      px1[j] = rho[1] * (px2[j] + gamma[1] * pz2[j]) + (1.0-rho[1]) * px[j];
      pr1[j] = rho[1] * (pr2[j] - gamma[1] * pw2[j]) + (1.0-rho[1]) * pr[j];
      pz1[j] = rho[1] * (pz2[j] - gamma[1] * pp2[j]) + (1.0-rho[1]) * pz[j];
      pw1[j] = rho[1] * (pw2[j] - gamma[1] * pq2[j]) + (1.0-rho[1]) * pw[j];
      pp1[j] = rho[1] * (pp2[j] - gamma[1] * pc2[j]) + (1.0-rho[1]) * pp[j];
      pq1[j] = rho[1] * (pq2[j] - gamma[1] * pd2[j]) + (1.0-rho[1]) * pq[j];
      pc1[j] = rho[1] * (pc2[j] - gamma[1] * pg[j])  + (1.0-rho[1]) * pc[j];
      pd1[j] = rho[1] * (pd2[j] - gamma[1] * ph[j])  + (1.0-rho[1]) * pd[j];

      px[j]  = rho[0] * (px1[j] + gamma[0]  * pz1[j]) + (1.0-rho[0]) * px2[j];
      pr[j]  = rho[0] * (pr1[j] - gamma[0]  * pw1[j]) + (1.0-rho[0]) * pr2[j];
      pz[j]  = rho[0] * (pz1[j] - gamma[0]  * pp1[j]) + (1.0-rho[0]) * pz2[j];
      pw[j]  = rho[0] * (pw1[j] - gamma[0]  * pq1[j]) + (1.0-rho[0]) * pw2[j];
      pp[j]  = rho[0] * (pp1[j] - gamma[0]  * pc1[j]) + (1.0-rho[0]) * pp2[j];
      pq[j]  = rho[0] * (pq1[j] - gamma[0]  * pd1[j]) + (1.0-rho[0]) * pq2[j];

      lambda[0] += pz[j]  * pw[j];  lambda[1] += pz[j]  * pq[j];
      lambda[2] += pz[j]  * pw1[j]; lambda[3] += pp[j]  * pq[j];
      lambda[4] += pp[j]  * pw1[j]; lambda[5] += pz1[j] * pw1[j];
      lambda[6] += pz[j]  * pr[j];  lambda[7] += pz[j]  * pr1[j];
      lambda[8] += pz1[j] * pr1[j];
    }
    lambda[9] = lambda[6];
  }

  // Restore arrays to vectors
  VECS_RESTORE_ARRAY_READ1(vx,px,px1,px2);  VECS_RESTORE_ARRAY_READ1(vr,pr,pr1,pr2);
  VECS_RESTORE_ARRAY_READ1(vz,pz,pz1,pz2);  VECS_RESTORE_ARRAY_READ1(vw,pw,pw1,pw2);
  VECS_RESTORE_ARRAY_READ1(vp,pp,pp1,pp2);  VECS_RESTORE_ARRAY_READ1(vq,pq,pq1,pq2);
  VECS_RESTORE_ARRAY_READ21(vc,pc,pc1,pc2); VECS_RESTORE_ARRAY_READ21(vd,pd,pd1,pd2);
  ierr = VecRestoreArrayRead(vg,(const PetscScalar**)&pg);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(vh,(const PetscScalar**)&ph);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/**********************************************************
 * Compute merged vecops and dot products for BiCGStab
 * x <- alpha * p + omega * s + x
 * r <- s - w t
 * rho = (r,rp)
 * dp = ||r||
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_BICGSTAB"
PetscErrorCode VecMergedOps_BICGSTAB(Vec X, Vec P,Vec S, Vec R, Vec T, Vec RP,
                                     PetscScalar alpha, PetscScalar omega,
                                     PetscScalar *rho,PetscScalar *dp)
{
  PetscScalar       * __restrict PX, * __restrict PR;
  const PetscScalar * __restrict PP, * __restrict PS;
  const PetscScalar * __restrict PT, * __restrict PRP;
  PetscScalar       sumrho = 0.0, sumdp = 0.0;
  PetscInt          i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  // Get arrays from vectors
  ierr = VecGetArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecGetArray(R,(PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetArrayRead(P, (const PetscScalar**)&PP); CHKERRQ(ierr);
  ierr = VecGetArrayRead(S, (const PetscScalar**)&PS); CHKERRQ(ierr);
  ierr = VecGetArrayRead(T, (const PetscScalar**)&PT); CHKERRQ(ierr);
  ierr = VecGetArrayRead(RP,(const PetscScalar**)&PRP);CHKERRQ(ierr);
  ierr = VecGetLocalSize(X,&n);CHKERRQ(ierr);

  // Compute merged vector operations
  #pragma _CRI ivdep
  for(i=0; i<n; i++) {
    PX[i]   = alpha*PP[i] + omega*PS[i] + PX[i];
    PR[i]   = PS[i] - omega*PT[i];
    sumrho += PR[i]*PRP[i];
    sumdp  += PR[i]*PR[i];
  }

  // Set local reduction values
  *rho = sumrho;
  *dp  = sumdp;

  // Restore arrays to vectors
  ierr = VecRestoreArray(X,(PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecRestoreArray(R,(PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(P, (const PetscScalar**)&PP); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(S, (const PetscScalar**)&PS); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T, (const PetscScalar**)&PT); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(RP,(const PetscScalar**)&PRP);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/**********************************************************
 * Compute merged vecops and dot products for PIPEBiCGStab
 * prior to first overlapped MatVec and PC
 * p2 <- r2 - beta * omega * s2 + beta * p2
 * s  <- w  - beta * omega * z + beta * s
 * s2 <- w2 - beta * omega * z2 + beta * s2
 * z  <- t  - beta * omega * v + beta * z
 * q  <- r  - alpha s
 * q2 <- r2 - alpha s2
 * y  <- w  - alpha z
 * d1 <- (q,y)
 * d2 <- (y,y)
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_PIPEBICGSTAB1"
PetscErrorCode VecMergedOps_PIPEBICGSTAB1(Vec P2, Vec Q, Vec Q2, Vec R, Vec R2, Vec S, Vec S2,
                                          Vec T, Vec V, Vec W, Vec W2, Vec Y, Vec Z, Vec Z2,
                                          PetscScalar alpha, PetscScalar beta, PetscScalar omega,
                                          PetscInt iter, PetscScalar *d1,PetscScalar *d2)
{
  PetscScalar       * __restrict PP2, * __restrict PQ, * __restrict PQ2, * __restrict PS;
  PetscScalar       * __restrict PS2, * __restrict PY, * __restrict PZ;
  const PetscScalar * __restrict PR, * __restrict PR2, * __restrict PT, * __restrict PV;
  const PetscScalar * __restrict PW, * __restrict PW2, * __restrict PZ2;
  PetscScalar       betaomega, sumd1 = 0.0, sumd2 = 0.0;
  PetscInt          i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  // Get arrays from vectors
  ierr = VecGetArray(P2,(PetscScalar**)&PP2);CHKERRQ(ierr);
  ierr = VecGetArray(Q, (PetscScalar**)&PQ); CHKERRQ(ierr);
  ierr = VecGetArray(Q2,(PetscScalar**)&PQ2);CHKERRQ(ierr);
  ierr = VecGetArray(S, (PetscScalar**)&PS); CHKERRQ(ierr);
  ierr = VecGetArray(S2,(PetscScalar**)&PS2);CHKERRQ(ierr);
  ierr = VecGetArray(Y, (PetscScalar**)&PY); CHKERRQ(ierr);
  ierr = VecGetArray(Z, (PetscScalar**)&PZ); CHKERRQ(ierr);
  ierr = VecGetArrayRead(R, (const PetscScalar**)&PR); CHKERRQ(ierr);
  ierr = VecGetArrayRead(R2,(const PetscScalar**)&PR2);CHKERRQ(ierr);
  ierr = VecGetArrayRead(T, (const PetscScalar**)&PT); CHKERRQ(ierr);
  ierr = VecGetArrayRead(V, (const PetscScalar**)&PV); CHKERRQ(ierr);
  ierr = VecGetArrayRead(W, (const PetscScalar**)&PW); CHKERRQ(ierr);
  ierr = VecGetArrayRead(W2,(const PetscScalar**)&PW2);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z2,(const PetscScalar**)&PZ2);CHKERRQ(ierr);
  ierr = VecGetLocalSize(P2,&n);CHKERRQ(ierr);

  // Compute merged vector operations for first iteration
  if(iter==0) {
    #pragma _CRI ivdep
    for(i=0; i<n; i++) {
      PP2[i] = PR2[i];
      PS[i]  = PW[i];
      PS2[i] = PW2[i];
      PZ[i]  = PT[i];
      PQ[i]  = PR[i]  - alpha*PS[i];
      PQ2[i] = PR2[i] - alpha*PS2[i];
      PY[i]  = PW[i]  - alpha*PZ[i];
      sumd1 += PQ[i]*PY[i];
      sumd2 += PY[i]*PY[i];
    }
  }
  // Compute merged vector operations for later iterations
  else {
    betaomega = beta*omega;
    #pragma _CRI ivdep
    for(i=0; i<n; i++) {
      PP2[i] = PR2[i] - betaomega*PS2[i] + beta*PP2[i];
      PS[i]  = PW[i]  - betaomega*PZ[i]  + beta*PS[i];
      PS2[i] = PW2[i] - betaomega*PZ2[i] + beta*PS2[i];
      PZ[i]  = PT[i]  - betaomega*PV[i]  + beta*PZ[i];
      PQ[i]  = PR[i]  - alpha*PS[i];
      PQ2[i] = PR2[i] - alpha*PS2[i];
      PY[i]  = PW[i]  - alpha*PZ[i];
      sumd1 += PQ[i]*PY[i];
      sumd2 += PY[i]*PY[i];
    }
  }

  // Set local reduction values
  *d1 = sumd1;
  *d2 = sumd2;

  // Restore arrays to vectors
  ierr = VecRestoreArray(P2,(PetscScalar**)&PP2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Q,(PetscScalar**)&PQ);CHKERRQ(ierr);
  ierr = VecRestoreArray(Q2,(PetscScalar**)&PQ2);CHKERRQ(ierr);
  ierr = VecRestoreArray(S,(PetscScalar**)&PS);CHKERRQ(ierr);
  ierr = VecRestoreArray(S2,(PetscScalar**)&PS2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Y,(PetscScalar**)&PY);CHKERRQ(ierr);
  ierr = VecRestoreArray(Z,(PetscScalar**)&PZ);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(R,(const PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(R2,(const PetscScalar**)&PR2);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T,(const PetscScalar**)&PT);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(V,(const PetscScalar**)&PV);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(W,(const PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(W2,(const PetscScalar**)&PW2);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z2,(const PetscScalar**)&PZ2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/**********************************************************
 * Compute merged vecops and dot products for PIPEBiCGStab
 * prior to second overlapped MatVec and PC
 * x <- alpha * p2 + omega * q2 + x
 * r <- q - omega y
 * r2 <- w2 - alpha z2
 * r2 <- q2 - omega r2
 * w <- t - alpha v
 * w <- y - omega w
 * rho <- (r,rp)
 * d1 <- (s,rp)
 * d2 <- (w,rp)
 * d3 <- (z,rp)
 * dp <- (r,r)
 *********************************************************/
#undef __FUNCT__
#define __FUNCT__ "VecMergedOps_PIPEBICGSTAB2"
PetscErrorCode VecMergedOps_PIPEBICGSTAB2(Vec P2, Vec Q, Vec Q2, Vec R, Vec R2,
                                          Vec RP, Vec S, Vec T, Vec V, Vec W,
                                          Vec W2, Vec X, Vec Y, Vec Z, Vec Z2,
                                          PetscScalar alpha, PetscScalar omega,
                                          PetscScalar *rho, PetscScalar *d1,
                                          PetscScalar *d2, PetscScalar *d3, PetscScalar *dp)
{
  PetscScalar       * __restrict PR,  * __restrict PR2, * __restrict PW,  * __restrict PX;
  const PetscScalar * __restrict PP2, * __restrict PQ,  * __restrict PQ2, * __restrict PRP;
  const PetscScalar * __restrict PS,  * __restrict PT,  * __restrict PV,  * __restrict PW2;
  const PetscScalar * __restrict PY,  * __restrict PZ,  * __restrict PZ2;
  PetscScalar       sumrho = 0.0, sumd1 = 0.0, sumd2 = 0.0;
  PetscScalar       sumd3 = 0.0,  sumdp = 0.0;
  PetscInt          i, n;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  // Get arrays from vectors
  ierr = VecGetArray(R, (PetscScalar**)&PR);CHKERRQ(ierr);
  ierr = VecGetArray(R2,(PetscScalar**)&PR2);CHKERRQ(ierr);
  ierr = VecGetArray(W, (PetscScalar**)&PW);CHKERRQ(ierr);
  ierr = VecGetArray(X, (PetscScalar**)&PX);CHKERRQ(ierr);
  ierr = VecGetArrayRead(P2,(const PetscScalar**)&PP2);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Q, (const PetscScalar**)&PQ); CHKERRQ(ierr);
  ierr = VecGetArrayRead(Q2,(const PetscScalar**)&PQ2);CHKERRQ(ierr);
  ierr = VecGetArrayRead(RP,(const PetscScalar**)&PRP);CHKERRQ(ierr);
  ierr = VecGetArrayRead(S, (const PetscScalar**)&PS); CHKERRQ(ierr);
  ierr = VecGetArrayRead(T, (const PetscScalar**)&PT); CHKERRQ(ierr);
  ierr = VecGetArrayRead(V, (const PetscScalar**)&PV); CHKERRQ(ierr);
  ierr = VecGetArrayRead(W2,(const PetscScalar**)&PW2);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Y, (const PetscScalar**)&PY); CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z, (const PetscScalar**)&PZ); CHKERRQ(ierr);
  ierr = VecGetArrayRead(Z2,(const PetscScalar**)&PZ2);CHKERRQ(ierr);
  ierr = VecGetLocalSize(P2,&n);CHKERRQ(ierr);

  // Compute merged vector operations
  #pragma _CRI ivdep
  for(i=0; i<n; i++) {
    PX[i]   = alpha*PP2[i] + omega*PQ2[i] + PX[i];
    PR[i]   = PQ[i]  - omega*PY[i];
    PR2[i]  = PQ2[i] - omega*(PW2[i] - alpha*PZ2[i]);
    PW[i]   = PY[i]  - omega*(PT[i]  - alpha*PV[i]);
    sumrho += PR[i]*PRP[i];
    sumd1  += PS[i]*PRP[i];
    sumd2  += PW[i]*PRP[i];
    sumd3  += PZ[i]*PRP[i];
    sumdp  += PR[i]*PR[i];
  }

  // Set local reduction values
  *rho = sumrho;
  *d1  = sumd1;
  *d2  = sumd2;
  *d3  = sumd3;
  *dp  = sumdp;

  // Restore arrays to vectors
  ierr = VecRestoreArray(R, (PetscScalar**)&PR); CHKERRQ(ierr);
  ierr = VecRestoreArray(R2,(PetscScalar**)&PR2);CHKERRQ(ierr);
  ierr = VecRestoreArray(W, (PetscScalar**)&PW); CHKERRQ(ierr);
  ierr = VecRestoreArray(X, (PetscScalar**)&PX); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(P2,(const PetscScalar**)&PP2);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Q, (const PetscScalar**)&PQ); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Q2,(const PetscScalar**)&PQ2);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(RP,(const PetscScalar**)&PRP);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(S, (const PetscScalar**)&PS); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T, (const PetscScalar**)&PT); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(V, (const PetscScalar**)&PV); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(W2,(const PetscScalar**)&PW2);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Y, (const PetscScalar**)&PY); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z, (const PetscScalar**)&PZ); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(Z2,(const PetscScalar**)&PZ2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
