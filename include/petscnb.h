#ifndef _PETSC_NB_H_
#define _PETSC_NB_H_

#include <mpi.h>
#include <sa_app.h>

typedef struct {

  PetscInt    use_nb;             // Are non-blocking operations being used?
  PetscInt    ntests;             // Number of requests during overlap
  PetscScalar mvmult, pcmult;     // Matvec and PC multipliers
  PetscInt    nmvcheck, npccheck; // How often to call MPI_Test()
  PetscInt    nmvtests, npctests; // Number of times MPI_Test() was called
  PetscMPIInt flg;                // Has operation completed?
  PetscInt    count;              // Number of operations since last test
  MPI_Request *req, *reqs;       // Request to test
  MPI_Status  *stat, *stats;     // MPI Status
} NonBlockingData;

/*S
   NB_Data - Holds data for non-blocking allreduce with MPI_Test()

   Level: advanced

S*/
NonBlockingData *NB_Data;

/*@
   NBTestMV - Check if MPI_Test() needs to be called for matrix-vector
              multiply and call if needed.

   Notes:
   Called from within matrix-vector multiply routines. Currently called
   by MatMult_SeqAIJ in aij.c and MatMult_SeqSBAIJ_1_Hermitian in relax.h.

   Level: advanced

.keywords KSP
 @*/
PETSC_STATIC_INLINE PetscErrorCode NBTestMV() {
  PetscErrorCode ierr;
  int rank;

  PetscFunctionBegin;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  if(NB_Data!=NULL && NB_Data->use_nb && !NB_Data->flg) {
    NB_Data->count++;
    if(NB_Data->count >= NB_Data->nmvcheck) {
      if(NB_Data->ntests>1) {
        NT_FUNC(ierr = MPI_Testall(NB_Data->ntests,NB_Data->reqs,&NB_Data->flg,NB_Data->stats),NTD_MVTEST);CHKERRQ(ierr);
      } else {
        NT_FUNC(ierr = MPI_Test(NB_Data->req,&NB_Data->flg,NB_Data->stat),NTD_MVTEST);CHKERRQ(ierr);
      }
      NB_Data->nmvtests++;
      NB_Data->count = 0;
    }
  }
  PetscFunctionReturn(0);
}

/*@
   NBTestPC - Check if MPI_Test() needs to be called for preconditioner
              application and call if needed.

   Notes:
   Called from within matrix-vector multiply and preconditioner routines.
   Currently called by MatSolve_SeqAIJ_NaturalOrdering in aijfact.c.

   Level: advanced

.keywords KSP
 @*/
PETSC_STATIC_INLINE PetscErrorCode NBTestPC() {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(NB_Data!=NULL && NB_Data->use_nb && !NB_Data->flg) {
    NB_Data->count++;
    if(NB_Data->count >= NB_Data->npccheck) {
      if(NB_Data->ntests>1) {
        NT_FUNC(ierr = MPI_Testall(NB_Data->ntests,NB_Data->reqs,&NB_Data->flg,NB_Data->stats),NTD_PCTEST);CHKERRQ(ierr);
      } else {
        NT_FUNC(ierr = MPI_Test(NB_Data->req,&NB_Data->flg,NB_Data->stat),NTD_PCTEST);CHKERRQ(ierr);
      }
      NB_Data->npctests++;
      NB_Data->count = 0;
    }
  }
  PetscFunctionReturn(0);
}

/*@
   NBCreate - Create struct for non-blocking allreduces using MPI_Test().

   Input Parameters:
+  nvecsize - Number of rows per core in matrix/vectors for overlap
-  nkernels - Number of matrix-vector multiply or preconditioner applicaiton
              kernels to overlap

   Options Database Keys:
+  -use_nb <use_nb>     - Use non-blocking allreduce with MPI_Test()
.  -nb_mv_mult <mvmult> - Set multiplier for how often to call MPI_Test() in
                          matrix-vector multiply. MPI_Test() is called
                          mvmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.
-  -nb_pc_mult <pcmult> - Set multiplier for how often to call MPI_Test() in
                          preconditioner application. MPI_Test() is called
                          pcmult*log_2(size)/nkernels times per kernel.
                          Default is 2.0.

   Notes:
   These routines are called by SAPCG, SANBPCG, SAPIPECG, and SAPIPE2CG solvers.
   NBTestMV() and NBTestPC() inline functions can be found in petscnb.h. These
   routines call MPI_Test() in matrix-vector multiply and preconditioner kernels.

   Level: advanced

.keywords: KSP
 @*/
PETSC_STATIC_INLINE PetscErrorCode NBCreate(PetscInt nvecsize,PetscInt nkernels) {
  PetscErrorCode ierr;
  PetscInt       use_nb, nmsgs, size;

  PetscFunctionBegin;
  // Get use_nb option
  use_nb = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-use_nb",&use_nb,NULL);CHKERRQ(ierr);

  if(use_nb) {
    ierr = PetscMalloc(sizeof(NonBlockingData),&NB_Data);CHKERRQ(ierr);
    NB_Data->use_nb = use_nb;
    MPI_Comm_size(PETSC_COMM_WORLD,&size);

    // Get other user options
    NB_Data->mvmult = 2.0; NB_Data->pcmult = 2.0;
    ierr = PetscOptionsGetScalar(NULL,NULL,"-nb_mv_mult",&NB_Data->mvmult,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(NULL,NULL,"-nb_pc_mult",&NB_Data->pcmult,NULL);CHKERRQ(ierr);

    // Determine how often to call MPI_Test for matvec
    nmsgs = (PetscInt)ceil(log2(size)*NB_Data->mvmult/nkernels);
    NB_Data->nmvcheck = (PetscInt)fmax(ceil((PetscScalar)nvecsize/(PetscScalar)(nmsgs+1)),1.0);

    // Determine how often to call MPI_Test for block-Jacobi PC
    nmsgs = (PetscInt)ceil(log2(size)*NB_Data->pcmult/nkernels);
    NB_Data->npccheck  = (PetscInt)fmax(ceil(2.0*(PetscScalar)nvecsize/(PetscScalar)(nmsgs+1)),1.0);

    // Init other variables
    NB_Data->ntests   = 1;
    NB_Data->nmvtests = 0;    NB_Data->npctests = 0;
    NB_Data->flg      = 1;    NB_Data->count    = 0;
    NB_Data->req      = NULL; NB_Data->stat     = NULL;
    NB_Data->reqs     = NULL; NB_Data->stats    = NULL;
  }
  PetscFunctionReturn(0);
}

/*@
   NBInit - Initialize struct for non-blocking allreduces using MPI_Test()

   Input Parameters:
+  req - MPI_Request used for non-blocking allreduce
-  stat - MPI_Status returned from MPI_Test()

   Notes:
   Called after MPI_Iallreduce() to setup struct for non-blocking allreduce using
   MPI_Test() for SAPCG, SANBPCG, SAPIPECG, and SAPIPE2CG solvers.

   Level: advanced

.keywords KSP
 @*/
PETSC_STATIC_INLINE PetscErrorCode NBInit(MPI_Request *req,MPI_Status *stat) {
  PetscFunctionBegin;
  if(NB_Data!=NULL && NB_Data->use_nb) {
    NB_Data->ntests   = 1;
    NB_Data->nmvtests = 0;
    NB_Data->npctests = 0;
    NB_Data->flg      = 0;
    NB_Data->count    = 0;
    NB_Data->req      = req;
    NB_Data->stat     = stat;
  }
  PetscFunctionReturn(0);
}

PETSC_STATIC_INLINE PetscErrorCode NBInitAll(MPI_Request **reqs,MPI_Status **stats,PetscInt ntests) {
  PetscFunctionBegin;
  if(NB_Data!=NULL && NB_Data->use_nb) {
    NB_Data->ntests   = ntests;
    NB_Data->nmvtests = 0;
    NB_Data->npctests = 0;
    NB_Data->flg      = 0;
    NB_Data->count    = 0;
    NB_Data->reqs     = *reqs;
    NB_Data->stats    = *stats;
  }
  PetscFunctionReturn(0);
}

PETSC_STATIC_INLINE PetscErrorCode NBFinish() {
  PetscFunctionBegin;
  if(NB_Data!=NULL && NB_Data->use_nb) {
    NB_Data->ntests   = 0;
    NB_Data->nmvtests = 0;
    NB_Data->npctests = 0;
    NB_Data->flg      = 1;
    NB_Data->count    = 0;
    NB_Data->req      = NULL;
    NB_Data->stat     = NULL;
  }
  PetscFunctionReturn(0);
}

/*@
   NBDestroy - Destroy struct for non-blocking allreduces using MPI_Test()

   Notes:
   Called at end of SAPCG, SANBPCG, SAPIPECG, and SAPIPE2CG solvers.

   Level: advanced

.keywords KSP
 @*/
PETSC_STATIC_INLINE PetscErrorCode NBDestroy() {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(NB_Data!=NULL && NB_Data->use_nb) {
    ierr = PetscFree(NB_Data);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/*PetscErrorCode NBCreate(PetscInt vecsize,PetscInt nkernels);
PetscErrorCode NBDestroy();
PetscErrorCode NBInit(MPI_Request *request,MPI_Status *stat);
PetscErrorCode NBInitAll(MPI_Request **reqs,MPI_Status **stats,PetscInt ntests);
 */
#endif
